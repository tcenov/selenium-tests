package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import common.Base;
import common.A00_HomePage;
import common.A13_Complete_page;

public class A13_Complete_test extends Base {

	@Test(priority = 1)
	void complete() throws InterruptedException {
		A00_HomePage homePage = new A00_HomePage(driver);
		homePage.enterEmail(userName);
		homePage.enterPassword(userPassword);
		homePage.verifyTextOnPage();
		homePage.clickLogInButton();

		A13_Complete_page completePage = new A13_Complete_page(driver);
		completePage.clickNextButton();
		completePage.verifyTextOnPage();
		completePage.clickNextButton();
		suspend(1);

	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("A13_Complete_test");
		}
	}
}
