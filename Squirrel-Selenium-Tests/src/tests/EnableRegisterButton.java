package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class EnableRegisterButton {

	@Test(priority = 1)
	public void login_error_cases() throws Exception {

		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.get("http://loadbalancer-fe-816679731.ap-southeast-2.elb.amazonaws.com/client/register");

		Thread.sleep(5000);


		WebElement button = driver.findElement(By.cssSelector(".btn.btn-success.btn-block"));
		((JavascriptExecutor) driver).executeScript("arguments[0].removeAttribute('disabled','disabled')", button);

		System.out.println("Sleep----------------------------------");
		Thread.sleep(15000);

	}

}
