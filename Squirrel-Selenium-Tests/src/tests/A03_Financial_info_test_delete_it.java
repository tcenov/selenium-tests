package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.Base;
import common.A00_HomePage;
import common.A03_Financial_info_page;
import common.A04_Investment_strategy_page;

public class A03_Financial_info_test_delete_it extends Base {

	A00_HomePage homePage = new A00_HomePage(driver);

	@Test(priority = 1)
	void successfulLoginAndEnterFinancialInfo() throws InterruptedException {

		homePage.enterEmail(userName);
		homePage.enterPassword(userPassword);
		homePage.verifyTextOnPage();
		homePage.clickLogInButton();

		suspend(5);
//		openStepByNumber(3);
		A03_Financial_info_page financialInfoPage = new A03_Financial_info_page(driver);
		
		financialInfoPage.verifyEnteredUserInformation();
		
	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("A03_Financial_info_test");
		}
	}
}
