package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.Base;
import common.A00_HomePage;

public class HomePageTest extends Base {

	A00_HomePage homePage = new A00_HomePage(driver);

	@Test(priority = 1)
	void successfulLogin() throws InterruptedException {
		
		homePage.enterEmail("test@test.com");
		homePage.enterPassword("passssssssss");
		homePage.verifyTextOnPage();
		homePage.clickLogInButton();
		
		homePage.clickRegisterButton();
		suspend(10);
	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("HomePageTest");
		}
	}
}
