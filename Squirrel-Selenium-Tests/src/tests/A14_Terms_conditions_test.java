package tests;

import java.io.IOException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import common.Base;
import common.A00_HomePage;
import common.A14_Terms_conditions_page;

public class A14_Terms_conditions_test extends Base {

	@Test(priority = 1)
	void terms_conditions() throws InterruptedException {
		A00_HomePage homePage = new A00_HomePage(driver);
		homePage.verifyTextOnPage();
		homePage.clickRegisterButton();
		homePage.clickTermsAndConditionLink();

		A14_Terms_conditions_page terms_conditions_page = new A14_Terms_conditions_page(driver);
		terms_conditions_page.verifyTextOnPage();

		suspend(5);
	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("A14_Terms_conditions_test");
		}
	}
}
