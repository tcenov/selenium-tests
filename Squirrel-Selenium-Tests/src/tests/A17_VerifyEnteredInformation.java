package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import common.Base;
import common.A00_HomePage;
import common.A01_Basic_info_page;
import common.A02_Additional_info_page;
import common.A03_Financial_info_page;
import common.A04_Investment_strategy_page;
import common.A05_Fund_info_page;
import common.A06_Identity_info_page;
import common.A07_TFN_info_page;
import common.A08_Requirements_page;
import common.A09_Fees_info_page;
import common.A10_Payment_info_page;
import common.A11_Insurance_info_page;

public class A17_VerifyEnteredInformation extends Base {

	@Test(priority = 1)
	void verifyEnteredInformation() throws InterruptedException {
		A00_HomePage homePage = new A00_HomePage(driver);
		homePage.enterEmail(userName);
		homePage.enterPassword(userPassword);
		homePage.verifyTextOnPage();
		homePage.clickLogInButton();

		suspend(4);

		A11_Insurance_info_page insuranceInfo = new A11_Insurance_info_page(driver);
		insuranceInfo.verifyTextOnPage();
		insuranceInfo.clickBackButton();
		
		A10_Payment_info_page paymentInfo = new A10_Payment_info_page(driver);
		paymentInfo.verifyTextOnPage();
		paymentInfo.clickBackButton();
		
		A09_Fees_info_page feesInfoPage = new A09_Fees_info_page(driver);
		feesInfoPage.verifyTextOnPage();
		feesInfoPage.clickBackButton();
		
		A08_Requirements_page requirementsPage = new A08_Requirements_page(driver);
		requirementsPage.verifyTextOnPage();
		requirementsPage.clickBackButton();
		
		A07_TFN_info_page tfnInfoPage = new A07_TFN_info_page(driver);
		tfnInfoPage.verifyTextOnPageAfterSubmit();
		tfnInfoPage.clickBackButton();		
		
		A06_Identity_info_page identityInfoPage = new A06_Identity_info_page(driver);
		identityInfoPage.verifyTextOnPageAndFirstPicture();
		identityInfoPage.verifyEnteredUserInformation();
		identityInfoPage.clickBackButton();
		
		A05_Fund_info_page fundInfoPage = new A05_Fund_info_page(driver);
		fundInfoPage.verifyTextOnPage();
		fundInfoPage.verifyEnteredUserInformation();
		fundInfoPage.clickBackButton();
		
		A04_Investment_strategy_page investment_strategy = new A04_Investment_strategy_page(driver);
		investment_strategy.verifyTextOnPage();
		suspend(1);
		investment_strategy.clickBackButton();
				
		A03_Financial_info_page financialInfoPage = new A03_Financial_info_page(driver);
		financialInfoPage.verifyEnteredUserInformation();
		financialInfoPage.clickBackButton();
		
		A02_Additional_info_page additionalInfoPage = new A02_Additional_info_page(driver);
		additionalInfoPage.verifyTextOnPage();
		additionalInfoPage.verifyEnteredUserInformation();
		additionalInfoPage.clickBackButton();
		
		A01_Basic_info_page basicInfoPage = new A01_Basic_info_page(driver);
		basicInfoPage.verifyEnteredUserInformation();
		basicInfoPage.verifyTextOnPage();
		
		
		basicInfoPage.clickNextButton();
		additionalInfoPage.clickNextButton();
		financialInfoPage.clickNextButton();
		investment_strategy.clickNextButton();
		fundInfoPage.clickNextButton();
		identityInfoPage.clickNextButton();
		tfnInfoPage.clickNextButton();
		requirementsPage.clickNextButton();
		feesInfoPage.clickNextButton();
		paymentInfo.clickNextButton();
		
		insuranceInfo.clickBackButton();
		suspend(1);
		paymentInfo.clickBackButton();
		suspend(1);
		feesInfoPage.clickBackButton();
		suspend(1);
		requirementsPage.clickBackButton();
		suspend(1);
		tfnInfoPage.clickBackButton();
		suspend(1);
//		identityInfoPage.clickBackButton();
//		suspend(1);
//		fundInfoPage.clickBackButton();
//		suspend(1);
//		investment_strategy.clickBackButton();
//		suspend(1);
//		financialInfoPage.clickBackButton();
//		suspend(1);
//		additionalInfoPage.clickBackButton();
		
//		suspend(1000);

	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("VerifyEnteredInformation");
		}
	}
}
