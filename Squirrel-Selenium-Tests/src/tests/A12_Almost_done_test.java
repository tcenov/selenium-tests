package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import common.Base;
import common.A00_HomePage;
import common.A12_Almost_done_page;

public class A12_Almost_done_test extends Base {

	@Test(priority = 1)
	void almostDone() throws InterruptedException {
		A00_HomePage homePage = new A00_HomePage(driver);
		homePage.enterEmail(userName);
		homePage.enterPassword(userPassword);
		homePage.verifyTextOnPage();
		homePage.clickLogInButton();

		suspend(4);
		
		A12_Almost_done_page almostDone = new A12_Almost_done_page(driver);
		almostDone.verifyTextOnPage();
//		almostDone.clickNextButton();
		suspend(1);

	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("A12_Almost_done_test");
		}
	}
}
