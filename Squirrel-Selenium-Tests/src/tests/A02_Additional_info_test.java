package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.Base;
import common.A00_HomePage;
import common.A02_Additional_info_page;
import common.A03_Financial_info_page;

public class A02_Additional_info_test extends Base {

	A00_HomePage homePage = new A00_HomePage(driver);

	@Test(priority = 1)
	void enterAdditionalInfo() throws InterruptedException {

		homePage.enterEmail(userName);
		homePage.enterPassword(userPassword);
		homePage.verifyTextOnPage();
		homePage.clickLogInButton();

		suspend(4);
		
		A02_Additional_info_page additionalInfoPage = new A02_Additional_info_page(driver);
		additionalInfoPage.verifyTextOnPage();
		additionalInfoPage.selectGender();
		additionalInfoPage.enterDayOfBirth();
		additionalInfoPage.enterPlaceOfBirth("Sofia, Bulgaria");
		suspend(1);
		additionalInfoPage.enterAddress();
		additionalInfoPage.verifyEnteredUserInformationBeforeSubmit();
		
		additionalInfoPage.clickNextButton();
		suspend(1);
		
		A03_Financial_info_page financialInfoPage = new A03_Financial_info_page(driver);
		financialInfoPage.clickBackButton();
		
		suspend(1);
		additionalInfoPage.verifyTextOnPage();
		additionalInfoPage.verifyEnteredUserInformation();

	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("A02_Additional_info_test");
		}
	}
}
