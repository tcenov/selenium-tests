package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import common.Base;
import common.A00_HomePage;
import common.A08_Requirements_page;
import common.A09_Fees_info_page;
import common.A10_Payment_info_page;

public class A09_Fees_info_test_delete_it extends Base {

	@Test(priority = 1)
	void feesInfo() throws InterruptedException {
		A00_HomePage homePage = new A00_HomePage(driver);
		homePage.enterEmail(userName);
		homePage.enterPassword(userPassword);
		homePage.verifyTextOnPage();
		homePage.clickLogInButton();

		suspend(3);
//		openStepByNumber(9);
		
		A09_Fees_info_page feesInfoPage = new A09_Fees_info_page(driver);
		feesInfoPage.verifyTextOnPage();
		feesInfoPage.clickBackButton();
		A08_Requirements_page requirementsPage = new A08_Requirements_page(driver);
		requirementsPage.clickNextButton();
		feesInfoPage.verifyTextOnPage();
		
		for (int i = 0; i < 30; i++) {
			feesInfoPage.clickBackButton();
			requirementsPage.clickNextButton();
			feesInfoPage.verifyTextOnPage();
		}
		

	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("A09_Fees_info_test");
		}
	}
}
