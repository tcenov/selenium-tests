package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import common.Base;
import common.A00_HomePage;
import common.A07_TFN_info_page;
import common.A08_Requirements_page;

public class A07_TFN_info_test extends Base {

	@Test(priority = 1)
	void tfnInfo() throws InterruptedException {
		A00_HomePage homePage = new A00_HomePage(driver);
		homePage.enterEmail(userName);
		homePage.enterPassword(userPassword);
		homePage.verifyTextOnPage();
		homePage.clickLogInButton();
		suspend(5);

		A07_TFN_info_page tfnInfoPage = new A07_TFN_info_page(driver);
		tfnInfoPage.enterFundName();
		tfnInfoPage.enterMemberNumber();
		tfnInfoPage.verifyEnteredUserInformationBeforeSubmit();
		suspend(2);
		tfnInfoPage.verifyTextOnPageBeforeSubmit();
		tfnInfoPage.selectCheckBoxes();
		tfnInfoPage.clickAddFund();
		tfnInfoPage.clickNextButton();
		
		suspend(1);
		A08_Requirements_page requirementsPage = new A08_Requirements_page(driver);
		requirementsPage.clickBackButton();
		suspend(1);
		
		tfnInfoPage.verifyTextOnPageAfterSubmit();
		
	}
	
	

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("A07_TFN_info_test");
		}
	}
}
