package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import common.Base;
import common.A00_HomePage;
import common.A00_RegisterPage;

public class A00_RegisterPageTest extends Base {

	String generatedEmail;

	@Test(priority = 1)
	void test() throws InterruptedException {
		
		A00_HomePage homePage = new A00_HomePage(driver);
		homePage.verifyTextOnPage();
		homePage.clickRegisterButton();
		suspend(2);
		
		A00_RegisterPage registerPage = new A00_RegisterPage(driver);
//		registerPage.enableAndClickOnRegisterButton();
		registerPage.verifyTextOnPage();
		
		generatedEmail = registerPage.generateEmail();
		registerPage.enterEmail(generatedEmail);	
		registerPage.enterPassword(userPassword);
		registerPage.clickIAgree();
		
		suspend(2);
		registerPage.enableAndClickOnRegisterButton();
		registerPage.verifyUserActivationText();
		suspend(4);
		registerPage.openEmail(generatedEmail);
		registerPage.clickActivationButton();
		
		registerPage.verifyActivatedUserText();
		suspend(1);
	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("A00_RegisterPageTest");
		}
	}
}
