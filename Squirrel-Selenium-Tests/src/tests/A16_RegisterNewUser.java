package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import common.Base;
import common.A00_HomePage;
import common.A00_RegisterPage;

public class A16_RegisterNewUser extends Base {

	@Test(priority = 1)
	void registerNewUser() throws InterruptedException {

		A00_HomePage homePage = new A00_HomePage(driver);
		homePage.verifyTextOnPage();
		homePage.clickRegisterButton();
		suspend(1);

		A00_RegisterPage registerPage = new A00_RegisterPage(driver);
		registerPage.verifyTextOnPage();
		// Error check
//		registerPage.enterEmail("testa@test.test");
//		registerPage.enterPassword(userPassword);
//		registerPage.clickIAgree();
//		registerPage.enableAndClickOnRegisterButton();
//		registerPage.verifyAlreadyRegisteredUserError();
		// register new user
		registerPage.enterEmail(userName);
		registerPage.enterPassword(userPassword);
//		registerPage.enableAndClickOnRegisterButton();

//		registerPage.verifyUserActivationText();
		suspend(2);
		registerPage.openEmail(userName);
		registerPage.clickActivationButton();
		registerPage.verifyActivatedUserText();
		
//		switchToBrowserTabByNumber(1);
//		registerPage.clickActivationButton();
//		switchToBrowserTabByNumber(2);
//		registerPage.verifyActivationFailedText();
//		
	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("A16_RegisterNewUser");
		}
	}
}
