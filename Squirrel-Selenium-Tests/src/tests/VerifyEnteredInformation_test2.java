package tests;

import java.io.IOException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import common.Base;
import common.A00_HomePage;
import common.A01_Basic_info_page;
import common.A02_Additional_info_page;
import common.A03_Financial_info_page;
import common.A04_Investment_strategy_page;

public class VerifyEnteredInformation_test2 extends Base {

	@Test(priority = 1)
	void verifyEnteredInformation() throws InterruptedException {
		A00_HomePage homePage = new A00_HomePage(driver);
		homePage.enterEmail(userName);
		homePage.enterPassword(userPassword);
		homePage.verifyTextOnPage();
		homePage.clickLogInButton();

		suspend(4);
		
		A04_Investment_strategy_page investment_strategy = new A04_Investment_strategy_page(driver);
		investment_strategy.verifyTextOnPage();
		suspend(1);
		investment_strategy.clickBackButton();
				
		A03_Financial_info_page financialInfoPage = new A03_Financial_info_page(driver);
		financialInfoPage.verifyEnteredUserInformation();
		financialInfoPage.clickBackButton();
		
		A02_Additional_info_page additionalInfoPage = new A02_Additional_info_page(driver);
		additionalInfoPage.verifyTextOnPage();
		additionalInfoPage.verifyEnteredUserInformation();
		additionalInfoPage.clickBackButton();
		
		A01_Basic_info_page basicInfoPage = new A01_Basic_info_page(driver);
		basicInfoPage.verifyEnteredUserInformation();
		basicInfoPage.verifyTextOnPage();
		
		basicInfoPage.clickNextButton();
		additionalInfoPage.clickNextButton();
		financialInfoPage.clickNextButton();

	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("VerifyEnteredInformation_test2");
		}
	}
}
