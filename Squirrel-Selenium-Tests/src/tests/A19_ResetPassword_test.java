package tests;

import java.io.IOException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import common.Base;
import common.A00_HomePage;

public class A19_ResetPassword_test extends Base {

	@Test(priority = 1)
	void resetPassword() throws InterruptedException {
		A00_HomePage homePage = new A00_HomePage(driver);

		homePage.clickResetPasswordLink();

		suspend(100);

	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("A19_ResetPassword");
		}
	}
}
