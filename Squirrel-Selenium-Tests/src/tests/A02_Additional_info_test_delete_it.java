package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.Base;
import common.A00_HomePage;
import common.A01_Basic_info_page;
import common.A02_Additional_info_page;
import common.A03_Financial_info_page;

public class A02_Additional_info_test_delete_it extends Base {

	A00_HomePage homePage = new A00_HomePage(driver);

	@Test(priority = 1)
	void enterAdditionalInfo() throws InterruptedException {

		homePage.enterEmail(userName);
		homePage.enterPassword(userPassword);
		homePage.verifyTextOnPage();
		homePage.clickLogInButton();

		suspend(4);

		A02_Additional_info_page additionalInfoPage = new A02_Additional_info_page(driver);

		additionalInfoPage.clickBackButton();
		suspend(1);

		A01_Basic_info_page basicInfoPage = new A01_Basic_info_page(driver);
		basicInfoPage.verifyTextOnPage();
		basicInfoPage.verifyEnteredUserInformation();

		for (int i = 0; i < 30; i++) {
//			suspend(1);
			basicInfoPage.clickNextButton();
//			suspend(1);
			additionalInfoPage.clickBackButton();
			basicInfoPage.verifyEnteredUserInformation();
		}
	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("A02_Additional_info_test");
		}
	}
}
