package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import common.Base;
import common.A00_HomePage;
import common.A05_Fund_info_page;
import common.A06_Identity_info_page;

public class A05_Fund_info_test extends Base {

	@Test(priority = 1)
	void fundInfo() throws InterruptedException {
		A00_HomePage homePage = new A00_HomePage(driver);
		homePage.enterEmail(userName);
		homePage.enterPassword(userPassword);
		homePage.verifyTextOnPage();
		homePage.clickLogInButton();

		suspend(5);
//		openStepByNumber(5);
		
		A05_Fund_info_page fundInfoPage = new A05_Fund_info_page(driver);
		fundInfoPage.verifyTextOnPage();
		fundInfoPage.enterFundName();
		fundInfoPage.selectCheckbox();
		fundInfoPage.inviteMember();
		
		fundInfoPage.clickNextButton();
		suspend(1);
		A06_Identity_info_page identityInfoPage = new A06_Identity_info_page(driver);
		identityInfoPage.clickBackButton();
		
		fundInfoPage.verifyTextOnPage();
		fundInfoPage.verifyEnteredUserInformation();
		
		fundInfoPage.verifyInvitedEmail("invited_from_"+ userName);
		suspend(1);

	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("A05_Fund_info_test");
		}
	}
}
