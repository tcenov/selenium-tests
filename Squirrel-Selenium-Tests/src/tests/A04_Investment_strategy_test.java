package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import common.Base;
import common.A00_HomePage;
import common.A04_Investment_strategy_page;
import common.A05_Fund_info_page;

public class A04_Investment_strategy_test extends Base {

	@Test(priority = 1)
	void successfulLoginAndCheckInvestmentStrategy() throws InterruptedException {
		A00_HomePage homePage = new A00_HomePage(driver);
		homePage.enterEmail(userName);
		homePage.enterPassword(userPassword);
		homePage.verifyTextOnPage();
		homePage.clickLogInButton();
		
		suspend(4);
		A04_Investment_strategy_page investment_strategy = new A04_Investment_strategy_page(driver);
//		
		
		investment_strategy.verifyTextOnPage();
		
		investment_strategy.clickNextButton();
		
		A05_Fund_info_page fundInfoPage = new A05_Fund_info_page(driver);
		fundInfoPage.clickBackButton();
		
		investment_strategy.verifyTextOnPage();
		
	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("A04_Investment_strategy_test");
		}
	}
}
