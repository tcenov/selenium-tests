package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import common.Base;
import common.A00_HomePage;
import common.A11_Insurance_info_page;

public class A11_Insurance_info_test extends Base {

	@Test(priority = 1)
	void insuranceInfo() throws InterruptedException {
		A00_HomePage homePage = new A00_HomePage(driver);
		homePage.enterEmail(userName);
		homePage.enterPassword(userPassword);
		homePage.verifyTextOnPage();
		homePage.clickLogInButton();

//		openStepByNumber(11);
		suspend(4);
		
		A11_Insurance_info_page insuranceInfo = new A11_Insurance_info_page(driver);
		insuranceInfo.verifyTextOnPage();
		insuranceInfo.selectCheckbox();
		insuranceInfo.clickNextButton();
		suspend(1);

	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("A11_Insurance_info_test");
		}
	}
}
