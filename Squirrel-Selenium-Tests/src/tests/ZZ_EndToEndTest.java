package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import common.Base;
import common.A00_HomePage;
import common.A00_RegisterPage;
import common.A01_Basic_info_page;
import common.A02_Additional_info_page;
import common.A03_Financial_info_page;
import common.A04_Investment_strategy_page;
import common.A05_Fund_info_page;
import common.A06_Identity_info_page;
import common.A07_TFN_info_page;
import common.A08_Requirements_page;
import common.A09_Fees_info_page;
import common.A10_Payment_info_page;
import common.A11_Insurance_info_page;
import common.A12_Almost_done_page;
import common.A13_Complete_page;

public class ZZ_EndToEndTest extends Base {

	@Test(priority = 1)
	void endToEndTest() throws InterruptedException {

		A00_HomePage homePage = new A00_HomePage(driver);
		homePage.verifyTextOnPage();
		homePage.clickRegisterButton();
		suspend(2);

		A00_RegisterPage registerPage = new A00_RegisterPage(driver);
		registerPage.verifyTextOnPage();
		// Error check
		registerPage.enterEmail("testa@test.test");
		registerPage.enterPassword(userPassword);
		registerPage.clickIAgree();
		registerPage.enableAndClickOnRegisterButton();
		registerPage.verifyAlreadyRegisteredUserError();
		// register new user
		registerPage.enterEmail(userName);
		registerPage.enterPassword(userPassword);
		registerPage.enableAndClickOnRegisterButton();

		registerPage.verifyUserActivationText();
		registerPage.openEmail(userName);
		registerPage.clickActivationButton();
		registerPage.verifyActivatedUserText();
		
		open(UrlTest);
//		homePage = new A00_HomePage(driver);
		homePage.enterEmail(userName);
		homePage.enterPassword(userPassword);
		homePage.verifyTextOnPage();
		homePage.clickLogInButton();

		A01_Basic_info_page basicInfoPage = new A01_Basic_info_page(driver);
		basicInfoPage.enterFirstName("Ivan-Asen");
		basicInfoPage.enterMiddleName("Middle");
		basicInfoPage.enterLastName("Last");
		basicInfoPage.enterMothersName("Scarlett O'Hara");
		basicInfoPage.enterPhoneNumber("12345678910");
		suspend(1);
		basicInfoPage.clickNextButton();
		
		A02_Additional_info_page additionalInfoPage = new A02_Additional_info_page(driver);
		additionalInfoPage.selectGender();
		additionalInfoPage.enterDayOfBirth();
		additionalInfoPage.enterPlaceOfBirth("Sofia, Bulgaria");
		additionalInfoPage.enterAddress();
		suspend(1);
		additionalInfoPage.clickNextButton();
		
		A03_Financial_info_page financialInfoPage = new A03_Financial_info_page(driver);
		financialInfoPage.enterOcupation();
		financialInfoPage.enterEmployer();
		financialInfoPage.enterTotalAnualIncome("7");
		financialInfoPage.enterBalance("50000");
		suspend(1);
		financialInfoPage.clickNextButton();
		
		A04_Investment_strategy_page investment_strategy = new A04_Investment_strategy_page(driver);
		investment_strategy.verifyTextOnPage();
		investment_strategy.clickNextButton();
		
		A05_Fund_info_page fundInfoPage = new A05_Fund_info_page(driver);
		fundInfoPage.verifyTextOnPage();
		fundInfoPage.enterFundName();
		fundInfoPage.inviteMember();
//		fundInfoPage.selectCheckbox();
		fundInfoPage.clickNextButton();
		
		A06_Identity_info_page identityInfoPage = new A06_Identity_info_page(driver);
		identityInfoPage.uploadDocuments();
		identityInfoPage.verifyTextOnPageAndFirstPicture();
		identityInfoPage.selectIDExpirationDate();
		identityInfoPage.enterIDNumber();
		identityInfoPage.clickNextButton();
		
		A07_TFN_info_page tfnInfoPage = new A07_TFN_info_page(driver);
		tfnInfoPage.enterFundName();
		tfnInfoPage.enterMemberNumber();
		tfnInfoPage.clickAddFund();
		suspend(2);
		tfnInfoPage.verifyTextOnPageBeforeSubmit();
		tfnInfoPage.verifyEnteredUserInformationBeforeSubmit();
		tfnInfoPage.selectCheckBoxes();
		tfnInfoPage.clickAddFund();
		tfnInfoPage.clickNextButton();
		
		A08_Requirements_page requirementsPage = new A08_Requirements_page(driver);
		requirementsPage.verifyTextOnPage();
		requirementsPage.selectCheckbox();
		requirementsPage.clickNextButton();
		
		A09_Fees_info_page feesInfoPage = new A09_Fees_info_page(driver);
		feesInfoPage.verifyTextOnPage();
		feesInfoPage.selectCheckbox();
		feesInfoPage.clickNextButton();
		
		A10_Payment_info_page paymentInfo = new A10_Payment_info_page(driver);
		paymentInfo.verifyTextOnPage();
		paymentInfo.selectCheckboxes();
		paymentInfo.clickNextButton();
		
		A11_Insurance_info_page insuranceInfo = new A11_Insurance_info_page(driver);
		insuranceInfo.verifyTextOnPage();
		insuranceInfo.selectCheckbox();
		insuranceInfo.clickNextButton();
		
		A12_Almost_done_page almostDone = new A12_Almost_done_page(driver);
		almostDone.verifyTextOnPage();
		almostDone.clickNextButton();
		
		A13_Complete_page completePage = new A13_Complete_page(driver);
		completePage.verifyTextOnPage();
		completePage.clickNextButton();
		
		open(UrlTest+ "logout");
		open(UrlTest);
		homePage = new A00_HomePage(driver);
		homePage.enterEmail(userName);
		homePage.enterPassword(userPassword);
		homePage.verifyTextOnPage();
		homePage.clickLogInButton();

		suspend(1);
		
//		completePage = new A13_Complete_page(driver);
		completePage.clickNextButton();
		completePage.verifyTextOnPage();
		suspend(1);
		
//		almostDone = new A12_Almost_done_page(driver);
		almostDone.verifyTextOnPage();
		suspend(1);
		almostDone.clickBackButton();
		
//		insuranceInfo = new A11_Insurance_info_page(driver);
		insuranceInfo.verifyTextOnPage();
		suspend(1);
		insuranceInfo.clickBackButton();
		
//		paymentInfo = new A10_Payment_info_page(driver);
		paymentInfo.verifyTextOnPage();
		suspend(1);
		paymentInfo.clickBackButton();
		
//		feesInfo = new A09_Fees_info_page(driver);
		feesInfoPage.verifyTextOnPage();
		suspend(1);
		feesInfoPage.clickBackButton();
		
		requirementsPage = new A08_Requirements_page(driver);
		requirementsPage.verifyTextOnPage();
		suspend(1);
		requirementsPage.clickBackButton();
		
		tfnInfoPage = new A07_TFN_info_page(driver);
		tfnInfoPage.verifyTextOnPageAfterSubmit();
		suspend(1);
		tfnInfoPage.clickBackButton();		
		
		identityInfoPage = new A06_Identity_info_page(driver);
		identityInfoPage.verifyTextOnPageAndFirstPicture();
		identityInfoPage.verifyEnteredUserInformation();
		suspend(1);
		identityInfoPage.clickBackButton();
		
		fundInfoPage = new A05_Fund_info_page(driver);
		fundInfoPage.verifyTextOnPage();
		fundInfoPage.verifyEnteredUserInformation();
		suspend(1);
		fundInfoPage.clickBackButton();
		
		investment_strategy = new A04_Investment_strategy_page(driver);
		investment_strategy.verifyTextOnPage();
		suspend(1);
		investment_strategy.clickBackButton();
				
		financialInfoPage = new A03_Financial_info_page(driver);
		financialInfoPage.verifyTextOnPage();
		financialInfoPage.verifyEnteredUserInformation();
		suspend(1);
		financialInfoPage.clickBackButton();
		
		additionalInfoPage = new A02_Additional_info_page(driver);
		additionalInfoPage.verifyTextOnPage();
		additionalInfoPage.verifyEnteredUserInformation();
		suspend(1);
		additionalInfoPage.clickBackButton();
		
		basicInfoPage = new A01_Basic_info_page(driver);
		basicInfoPage.verifyEnteredUserInformation();
		basicInfoPage.verifyTextOnPage();
		basicInfoPage.clickNextButton();
		
		additionalInfoPage.clickNextButton();
		financialInfoPage.clickNextButton();
		investment_strategy.clickNextButton();
		fundInfoPage.clickNextButton();
		identityInfoPage.clickNextButton();
		tfnInfoPage.clickNextButton();
		requirementsPage.clickNextButton();
		feesInfoPage.clickNextButton();
		paymentInfo.clickNextButton();
		insuranceInfo.clickNextButton();
		almostDone.clickNextButton();
		completePage.clickNextButton();
		
		
		suspend(5);
	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("endToEndTest");
		}
	}
}
