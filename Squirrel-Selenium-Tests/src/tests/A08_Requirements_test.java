package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import common.Base;
import common.A00_HomePage;
import common.A08_Requirements_page;
import common.A09_Fees_info_page;

public class A08_Requirements_test extends Base {

	@Test(priority = 1)
	void requirements() throws InterruptedException {
		A00_HomePage homePage = new A00_HomePage(driver);
		homePage.enterEmail(userName);
		homePage.enterPassword(userPassword);
		homePage.verifyTextOnPage();
		homePage.clickLogInButton();

		suspend(4);
		
		A08_Requirements_page requirementsPage = new A08_Requirements_page(driver);
		requirementsPage.verifyTextOnPage();
		requirementsPage.selectCheckbox();
		requirementsPage.clickNextButton();

		A09_Fees_info_page feesInfoPage = new A09_Fees_info_page(driver);
		feesInfoPage.clickBackButton();
		
		requirementsPage.verifyTextOnPage();

	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("A08_Requirements_test");
		}
	}
}
