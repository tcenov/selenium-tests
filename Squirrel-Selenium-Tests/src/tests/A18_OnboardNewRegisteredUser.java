package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.A00_HomePage;
import common.A01_Basic_info_page;
import common.A02_Additional_info_page;
import common.A03_Financial_info_page;
import common.A04_Investment_strategy_page;
import common.A05_Fund_info_page;
import common.A06_Identity_info_page;
import common.Base;

public class A18_OnboardNewRegisteredUser extends Base {

	@Test(priority = 1)
	void successfulLoginAndFinishOnboardingProcess() throws InterruptedException {

		

	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("A18_OnboardNewRegisteredUser");
		}
	}
}
