package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.Base;
import common.A00_HomePage;
import common.A00_RegisterPage;

public class A00_RegisterPage_create100Users extends Base {
	private String UrlTest = "http://ec2-54-153-249-209.ap-southeast-2.compute.amazonaws.com/client/login";
	private String generatedEmail;

	@Test(priority = 1)
	private void test() throws InterruptedException {

		A00_HomePage homePage = new A00_HomePage(driver);
		homePage.verifyTextOnPage();
		homePage.clickRegisterButton();
		suspend(2);

		A00_RegisterPage registerPage = new A00_RegisterPage(driver);
		// registerPage.enableAndClickOnRegisterButton();
		registerPage.verifyTextOnPage();

		// generatedEmail = registerPage.generateEmail();
		// registerPage.enterEmail(generatedEmail);
		// registerPage.enterPassword(userPassword);
		// registerPage.clickIAgree();

		suspend(1);
		// registerPage.enableAndClickOnRegisterButton();

		suspend(1);
		// registerPage.openEmail(generatedEmail);
		registerPage.openEmail(userName);
		registerPage.clickActivationButton();

		registerPage.verifyUserActivationText();
		suspend(1);
	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("A00_RegisterPage_create100Users");
		}
	}
}
