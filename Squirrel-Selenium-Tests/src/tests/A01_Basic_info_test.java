package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import common.A01_Basic_info_page;
import common.A02_Additional_info_page;
import common.Base;
import common.A00_HomePage;

public class A01_Basic_info_test extends Base {

	@Test(priority = 1)
	void enterBasicInfo() throws InterruptedException {

		A00_HomePage homePage = new A00_HomePage(driver);
		homePage.enterEmail(userName);
		homePage.enterPassword(userPassword);
		homePage.verifyTextOnPage();
		homePage.clickLogInButton();
		suspend(4);
		
		A01_Basic_info_page basicInfoPage = new A01_Basic_info_page(driver);
		basicInfoPage.verifyTextOnPage();
		basicInfoPage.enterFirstName("Ivan-Asen");
		basicInfoPage.enterMiddleName("Middle");
		basicInfoPage.enterLastName("Last");
		basicInfoPage.enterMothersName("Scarlett O'Hara");
		basicInfoPage.enterPhoneNumber("12345678910");
		
		basicInfoPage.verifyEnteredUserInformation();
		basicInfoPage.clickNextButton();
		suspend(1);
		
		A02_Additional_info_page additionalInfoPage = new A02_Additional_info_page(driver);
		additionalInfoPage.clickBackButton();
		
		basicInfoPage.verifyEnteredUserInformation();
		basicInfoPage.verifyTextOnPage();
		basicInfoPage.clickNextButton();
	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("A01_Basic_info_test");
		}
	}
}
