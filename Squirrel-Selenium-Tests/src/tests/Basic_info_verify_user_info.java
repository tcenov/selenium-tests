package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import common.Base;
import common.A00_HomePage;
import common.A01_Basic_info_page;

public class Basic_info_verify_user_info extends Base {

	A00_HomePage homePage = new A00_HomePage(driver);

	@Test(priority = 1)
	void successfulLoginAndEnterBasicInfo() throws InterruptedException {
		
		homePage.enterEmail(userName);
		homePage.enterPassword(userPassword);
		homePage.verifyTextOnPage();
		homePage.clickLogInButton();
		
		A01_Basic_info_page basic_info_page = new A01_Basic_info_page(driver);
		
//		registerPage.clickBackButton();
//		suspend(5);
		basic_info_page.verifyEnteredUserInformation();
		
		
		
		
		suspend(10);
//		registerPage.clickNextButton();
		
		suspend(10);
	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("Basic_info_verify_user_info");
		}
	}
}
