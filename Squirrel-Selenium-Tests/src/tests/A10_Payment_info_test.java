package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import common.Base;
import common.A00_HomePage;
import common.A10_Payment_info_page;
import common.A11_Insurance_info_page;

public class A10_Payment_info_test extends Base {

	@Test(priority = 1)
	void paymentInfo() throws InterruptedException {
		A00_HomePage homePage = new A00_HomePage(driver);
		homePage.enterEmail(userName);
		homePage.enterPassword(userPassword);
		homePage.verifyTextOnPage();
		homePage.clickLogInButton();

		suspend(4);
		
		A10_Payment_info_page paymentInfo = new A10_Payment_info_page(driver);
		paymentInfo.verifyTextOnPage();
		paymentInfo.selectCheckboxes();
		paymentInfo.clickNextButton();
		
		A11_Insurance_info_page insuranceInfo = new A11_Insurance_info_page(driver);
		insuranceInfo.clickBackButton();
		
		paymentInfo.verifyTextOnPage();

	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("A10_Payment_info_test");
		}
	}
}
