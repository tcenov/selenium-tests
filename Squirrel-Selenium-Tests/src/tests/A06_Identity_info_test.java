package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import common.Base;
import common.A00_HomePage;
import common.A06_Identity_info_page;
import common.A07_TFN_info_page;

public class A06_Identity_info_test extends Base {

	@Test(priority = 1)
	void identityInfo() throws InterruptedException {
		A00_HomePage homePage = new A00_HomePage(driver);
		homePage.enterEmail(userName);
		homePage.enterPassword(userPassword);
		homePage.verifyTextOnPage();
		homePage.clickLogInButton();

		suspend(4);
		A06_Identity_info_page identityInfoPage = new A06_Identity_info_page(driver);
//		openStepByNumber(6);
		
		identityInfoPage.uploadDocuments();
		identityInfoPage.selectIDExpirationDate();
		identityInfoPage.enterIDNumber();
		identityInfoPage.verifyTextOnPageAndFirstPicture();
		
		suspend(1);
		identityInfoPage.clickNextButton();
		suspend(1);
		
		A07_TFN_info_page tfnInfoPage = new A07_TFN_info_page(driver);
		tfnInfoPage.clickBackButton();
		
		identityInfoPage.verifyTextOnPageAndFirstPicture();
		identityInfoPage.verifyEnteredUserInformation();
	}

	@AfterMethod
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
		if (testResult.getStatus() == ITestResult.FAILURE) {
			takeScreenShotOnFailure("A06_Identity_info_test");
		}
	}
}
