package common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class A02_Additional_info_page extends Base {
	//client/onboarding/additional_info
	private By male = By.xpath("//option[@value='MALE']");
	private By calendar = By.cssSelector(".mat-calendar-body-cell-content");
	private By genderField = By.cssSelector(".ng-pristine.used.ng-valid");
	// private By container = By.cssSelector(".contain-container");
	private By addressFromGoogle = By.xpath("//*[contains(text(), 'Gepps')]");
	

	protected WebDriver driver;

	public A02_Additional_info_page(WebDriver driver) {
		this.driver = driver;
	}

	public void selectGender() {
		suspend(1);
		clickOnElementByNextNumber(additionalInfoInputFields, 1);
		suspend(1);
		click(male);
	}

	public void enterDayOfBirth() {
		// suspend(1);
		clickOnElementByNextNumber(additionalInfoInputFields, 2);
		click(calendar);
		click(calendar);
		click(calendar);
	}

	public void enterPlaceOfBirth(String place) {
		suspend(1);
		testTextFromListOfWebelements(additionalInfoInputFields);
		// clickOnElementByNextNumber(additionalInfoInputFields, 3);
		enterTextInElementByNextNumber(additionalInfoInputFields, 3, place);
		click(container);
	}

	public void enterAddress() {
		enterTextInElementByNextNumber(additionalInfoInputFields, 4, "Princes Highway,");
		click(addressFromGoogle);
	}

	public void verifyTextOnPage() {
		verifyText(container,
				"We will need a bit more\nFemale\nMale\nGender\nDay of birth\nPlace of birth\nAddress\nPostcode\nNext");
		verifyText(toolbar, "Self-Managed Super Fund");
		verifyText(backButtonText, "Back");
	}

	public void verifyEnteredUserInformation() {
		suspend(1);
//		testTextFromListOfWebelements(genderField);
		verifyValueTextFromInputs(genderField, "MALE");
		verifyValueTextFromInputsByNextNumber(inputFields, 1, "January 1, 1992");
		verifyValueTextFromInputsByNextNumber(inputFields, 2, "Sofia, Bulgaria");
		verifyValueTextFromInputsByNextNumber(inputFields, 3, "Princes Hwy, Gepps Cross SA 5094, Australia");
		verifyValueTextFromInputsByNextNumber(inputFields, 4, "5094");
	}
	
	public void verifyEnteredUserInformationBeforeSubmit() {
		verifyValueTextFromInputsByNextNumber(inputFields, 1, "January 1, 1992");
		verifyValueTextFromInputsByNextNumber(inputFields, 2, "Sofia, Bulgaria");
		verifyValueTextFromInputsByNextNumber(inputFields, 3, "Princes Highway, Gepps Cross SA, Australia");
	}
	
	private void progressBarCheck(By step) {
		CompareColorsOfWebElement(step, "rgba(253, 126, 69, 1)");
		print("you are on the right step");
	}

	public void clickNextButton() {
		progressBarCheck(step2);
		progressBarCheck(currentProgressBrick);
		clickOn(nextButton);
		waitForLoaderSpinner();
		progressBarCheck(step3);
	}

	public void clickBackButton() {
		progressBarCheck(step2);
		hoverMouseAndClick(backButton);
		progressBarCheck(step1);
	}
}
