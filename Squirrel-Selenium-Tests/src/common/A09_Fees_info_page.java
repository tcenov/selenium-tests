package common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class A09_Fees_info_page extends Base {
	//client/onboarding/fees_info
	protected WebDriver driver;

	public A09_Fees_info_page(WebDriver driver) {
		this.driver = driver;
	}

	public void verifyTextOnPage() {
		testTextFromListOfWebelements(container);
		verifyText(container, "Now to recap everything that�s included in our SMSF Setup and Admin.\n"
		+ "Our SMSF setup includes:\n"
		+ "Establishment and registration of SMSF Trust\n"
		+ "Establishment and registration of Special Purpose Corporate Trustee Company\n"
		+ "Establishment of SMSF governing rules\n"
		+ "Establishment and registration of investment strategy\n"
		+ "Submission of \"Election to be regulated\" Complience statement\n"
		+ "Establishment of accounts with service providers\n"
		+ "Set up of Bare trust and Bare trustee when required for the purchase of property\n"
		+ "Registration with Australian tax office\n"
		+ "Rollover other superannuation funds into SMSF\n"
		+ "Total cost:\n"
		+ "$3171.05 (incl. GST)\n"
		+ "Our SMSF Admin includes:\n"
		+ "Annual complience, administration and maintenance of SMSF\n"
		+ "Annual return of Special Purpose Company\n"
		+ "24/7 access to Squirrel online SMSF management portal\n"
		+ "Business hour access to Squirrel technical experts\n"
		+ "Fixed fee for 1-4 members and any number / value of assets\n"
		+ "Unlimited customer service\n"
		+ "Total cost:\n"
		+ "$121 per month (incl. GST)\n"
		+ "Remember: There is an annual $259 ATO supervisory levy payable when we lodge your tax return. This fee is not part of our control and is decided by the ATO.\n"
		+ "I accept and understand the fees\n"
		+ "Next");
		
		
		verifyPartialText(container, "Now to recap everything that");
		verifyPartialText(container, "included in our SMSF Setup and Admin.");
		verifyPartialText(container, "Our SMSF setup includes:");
		verifyPartialText(container, "Establishment and registration of SMSF Trust");
		verifyPartialText(container, "Establishment and registration of Special Purpose Corporate Trustee Company");
		verifyPartialText(container, "Establishment of SMSF governing rules");
		verifyPartialText(container, "Establishment and registration of investment strategy");
		verifyPartialText(container, "Submission of \"Election to be regulated\" Complience statement");
		verifyPartialText(container, "Establishment of accounts with service providers");
		verifyPartialText(container,
				"Set up of Bare trust and Bare trustee when required for the purchase of property");
		verifyPartialText(container, "Registration with Australian tax office");
		verifyPartialText(container, "Rollover other superannuation funds into SMSF");
		verifyPartialText(container, "Total cost:");
		verifyPartialText(container, "(incl. GST)");
		verifyPartialText(container, "Our SMSF Admin includes:");
		verifyPartialText(container, "Annual complience, administration and maintenance of SMSF");
		verifyPartialText(container, "Annual return of Special Purpose Company");
		verifyPartialText(container, "24/7 access to Squirrel online SMSF management portal");
		verifyPartialText(container, "Business hour access to Squirrel technical experts");
		verifyPartialText(container, "Fixed fee for 1-4 members and any number / value of assets");
		verifyPartialText(container, "Unlimited customer service");
		verifyPartialText(container, "Total cost:");
		verifyPartialText(container, "per month (incl. GST)");
		verifyPartialText(container,"Remember: There is an annual"); 
		verifyPartialText(container, "ATO supervisory levy payable when we lodge your tax return. This fee is not part of our control and is decided by the ATO.");
		verifyPartialText(container, "I accept and understand the fees");
		verifyPartialText(container, "Next");
		verifyText(toolbar, "Self-Managed Super Fund");
		verifyText(backButtonText, "Back");
	}

	public void selectCheckbox() {
		clickOn(checkBoxAgree);
	}
	
	private void progressBarCheck(By step) {
		CompareColorsOfWebElement(step, "rgba(253, 126, 69, 1)");
		print("you are on the right step");
	}

	public void clickNextButton() {
		progressBarCheck(currentProgressBrick);
		if (userName.startsWith("invited_from_")) {
			progressBarCheck(step7);
			clickOn(nextButton);
			waitForLoaderSpinner();
			progressBarCheck(step8);
		} else {
			progressBarCheck(step9);
			clickOn(nextButton);
			waitForLoaderSpinner();
			progressBarCheck(step10);
		}
		progressBarCheck(currentProgressBrick);
	}

	public void clickBackButton() {
		progressBarCheck(currentProgressBrick);
		if (userName.startsWith("invited_from_")) {
			progressBarCheck(step7);
			hoverMouseAndClick(backButton);
			waitForLoaderSpinner();
			progressBarCheck(step6);
		} else {
			progressBarCheck(step9);
			hoverMouseAndClick(backButton);
			waitForLoaderSpinner();
			progressBarCheck(step8);
		}
		progressBarCheck(currentProgressBrick);
	}
	
}
