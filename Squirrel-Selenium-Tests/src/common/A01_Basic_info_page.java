package common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class A01_Basic_info_page extends Base {
	// client/onboarding/basic_info
	protected WebDriver driver;

	public A01_Basic_info_page(WebDriver driver) {
		this.driver = driver;
	}

	public void enterFirstName(String name) {
		suspend(2);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.numberOfElementsToBe(inputFields, 5));
		wait.until(ExpectedConditions.elementToBeClickable(inputFields));
		enterTextInElementByNextNumber(inputFields, 1, name);
	}

	public void enterMiddleName(String name) {
		enterTextInElementByNextNumber(inputFields, 2, name);
	}

	public void enterLastName(String name) {
		enterTextInElementByNextNumber(inputFields, 3, name);
	}

	public void enterMothersName(String name) {
		enterTextInElementByNextNumber(inputFields, 4, name);
	}

	public void enterPhoneNumber(String phone) {
		enterTextInElementByNextNumber(inputFields, 5, phone);
	}

	public void verifyEnteredUserInformation() {
		suspend(1);
		verifyValueTextFromInputsByNextNumber(inputFields, 1, "Ivan-Asen");
		verifyValueTextFromInputsByNextNumber(inputFields, 2, "Middle");
		verifyValueTextFromInputsByNextNumber(inputFields, 3, "Last");
		verifyValueTextFromInputsByNextNumber(inputFields, 4, "Scarlett O'Hara");
		verifyValueTextFromInputsByNextNumber(inputFields, 5, "12345678910");
	}

	public void verifyTextOnPage() {
		verifyText(container,
				"Personal Information\nFirst name\nMiddle name\nLast name\nMother's maiden name\nPhone number\nNext");
		verifyText(toolbar, "Self-Managed Super Fund");
	}

	private void progressBarCheck(By step) {
		CompareColorsOfWebElement(step, "rgba(253, 126, 69, 1)");
		print("you are on the right step");
	}

	public void clickNextButton() {
		progressBarCheck(step1);
		progressBarCheck(currentProgressBrick);
		clickOn(nextButton);
		waitForLoaderSpinner();
		progressBarCheck(step2);
		progressBarCheck(currentProgressBrick);

	}
}
