package common;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class A14_Terms_conditions_page extends Base {
	protected WebDriver driver;

	public A14_Terms_conditions_page(WebDriver driver) {
		this.driver = driver;
	}

	public void verifyTextOnPage() {
		selectNewOpenedWindow();
		verifyText(backButtonText, "Back");
		verifyText(toolbar, "Self-Managed Super Fund");
		String ourTerms = getText(container);
//		suspend(4);
		ourTerms = ourTerms.replaceAll("�", "'");
		ourTerms = ourTerms.replaceAll("�", "\"");
		ourTerms = ourTerms.replaceAll("�", "\"");
		ourTerms = ourTerms.replaceAll("�", "'");
		ourTerms = ourTerms.replaceAll("�", "-");
		ourTerms = ourTerms.replaceAll("Third-Party Services \\(collectively referred to the Services\\)", "Third-Party Services\n(collectively referred to the Services)");
		
		
		print(ourTerms);
				
		open("https://www.squirrelsuper.com.au/terms-and-conditions/");
//		selectNewOpenedWindow();
		suspend(1);
		String squirrelTems = getText(squirrelContainer);
		suspend(1);
		squirrelTems = squirrelTems.replaceAll("�", "'");
		squirrelTems = squirrelTems.replaceAll("�", "'");
		squirrelTems = squirrelTems.replaceAll("�", "-");
		squirrelTems = squirrelTems.replaceAll("�", "\"");
		squirrelTems = squirrelTems.replaceAll("�", "\"");
		squirrelTems = squirrelTems.replaceAll("TERMS AND CONDITIONS OF USE", "TERMS AND CONDITIONS OF USE\n");
		squirrelTems = squirrelTems.replaceAll("\\(the Establishment Services\\)", "The Establishment Services");
		squirrelTems = squirrelTems.replaceAll("\\(the Facilitation Services\\)", "The Facilitation Services");
		squirrelTems = squirrelTems.replaceAll("\\(the Administration Services\\).", "The Administration Services");
		squirrelTems = squirrelTems.replaceAll("\\(the Sign-up Activities\\)", "The Sign-up Activities");
		squirrelTems = squirrelTems.replaceAll("\\(collectively referred to as the Fees\\)", "Collectively referred to as the Fees");
		squirrelTems = squirrelTems.replaceAll("Servicesspecified", "Services specified");
		squirrelTems = squirrelTems.replaceAll("\\(Third-Party Services\\)", "Third-Party Services");
		squirrelTems = squirrelTems.replaceAll("Business hour access to Squirrel customer support", "Business hour access to Squirrel customer support.");
		squirrelTems = squirrelTems.replaceAll("these Terms or the Squirrel Platforms and their performance. ", "these Terms or the Squirrel Platforms and their performance.");
		squirrelTems = squirrelTems.replaceAll("situation, or needs.  Please speak with a professional", "situation, or needs. Please speak with a professional");
		squirrelTems = squirrelTems.replaceAll("If you do not accept  the Terms", "If you do not accept the Terms");
		squirrelTems = squirrelTems.replaceAll("requested \\(the ATO Registration\\); and\\s", "requested (the ATO Registration); and");
		squirrelTems = squirrelTems.replaceAll("requested \\(the ATO Registration\\); and ", "requested (the ATO Registration); and");

		//bugs
		squirrelTems = squirrelTems.replaceAll("terms and conditions located at https://www.macquarie.com/au/advisers/solutions/self-managed-super-funds/. You acknowledge", "terms and conditions. You acknowledge");
		squirrelTems = squirrelTems.replaceAll("located https://www.hellosign.com/legal/tos.", "located ToS.");
		squirrelTems = squirrelTems.replaceAll("are found on our website https://www.squirrelsuper.com.au  \\(the Website\\).", "are found on our website the Website .");
		squirrelTems = squirrelTems.replaceAll("on the Website at https://www.squirrelsuper.com.au/privacy-policy/ \\(the Privacy Policy\\). You consent", "on the Website at the Privacy Policy . You consent");
		squirrelTems = squirrelTems.replaceAll("", "");
		print(squirrelTems);
		
		//switchToTopWindow();
		Assert.assertEquals(ourTerms, squirrelTems);
		print("verified text on page");
		
		verifyText(backButtonText, "Back");
	}

}
