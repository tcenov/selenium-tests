package common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class A03_Financial_info_page extends Base {
	//client/onboarding/financial_info
	private By ocupation = By.xpath("//*[contains(text(), 'Design')]");
	private By ocupationField = By.cssSelector(".used");
	protected WebDriver driver;

	public A03_Financial_info_page(WebDriver driver) {
		this.driver = driver;
	}

	public void enterOcupation() {
		clickOnElementByNextNumber(additionalInfoInputFields, 1);
		click(ocupation);
	}

	public void enterEmployer() {
		enterTextInElementByNextNumber(additionalInfoInputFields, 2, "Squirrel");
	}

	public void enterTotalAnualIncome(String number) {
		enterTextInElementByNextNumber(additionalInfoInputFields, 3, number);
	}

	public void enterBalance(String number) {
		enterTextInElementByNextNumber(additionalInfoInputFields, 4, number);
	}

	public void verifyTextOnPage() {
		verifyText(ocupationField, "Advertising, Public Relations and Sales Managers\n"
				+ "Business Administration Managers\n"
				+ "Chief Executives, General Managers and Legislators\n"
				+ "Construction, Distribution and Production Managers\n"
				+ "Education, Health and Welfare Services Managers\n"
				+ "Farmers and Farm Managers\n"
				+ "Hospitality, Retail and Service Managers\n"
				+ "Information & Communication Technology Managers\n"
				+ "Miscellaneous Specialist Managers\n"
				+ "Accountants, Auditors and Company Secretaries\n"
				+ "Arts and Media Professionals\n"
				+ "Business, Human Resource and Marketing Professionals\n"
				+ "Design, Engineering, Science and Transport Professionals\n"
				+ "Education Professionals\n"
				+ "Financial Brokers and Dealers, and Investment Advisers\n"
				+ "Doctor, Veterinarian, Health Professionals\n"
				+ "Information & Communication Technology Professionals\n"
				+ "Legal, Social and Welfare Professionals\n"
				+ "Automotive and Engineering Trades Workers\n"
				+ "Construction Trades Workers\n"
				+ "Electrotechnology and Telecommunications Trades Workers\n"
				+ "Engineering and Science Technicians\n"
				+ "Food Trades Workers\n"
				+ "Information & Communication Technology Technicians\n"
				+ "Other Technicians and Trades Workers\n"
				+ "Skilled Animal and Horticultural Workers\n"
				+ "Carers and Aides\n"
				+ "Health and Welfare Support Workers\n"
				+ "Hospitality Workers\n"
				+ "Protective Service Workers\n"
				+ "Sports and Personal Service Workers\n"
				+ "Clerical and Office Support Workers\n"
				+ "General Clerical Workers\n"
				+ "Inquiry Clerks and Receptionists\n"
				+ "Numerical Clerks\n"
				+ "Office Managers and Program Administrators\n"
				+ "Clerical and Administrative Workers\n"
				+ "Personal Assistants and Secretaries\n"
				+ "Sales Assistants and Salespersons\n"
				+ "Sales Representatives and Agents\n"
				+ "Sales Support Workers\n"
				+ "Machine and Stationary Plant Operators\n"
				+ "Mobile Plant Operators\n"
				+ "Road and Rail Drivers\n"
				+ "Storepersons\n"
				+ "Cleaners and Laundry Workers\n"
				+ "Construction and Mining Labourers\n"
				+ "Factory Process Workers\n"
				+ "Farm, Forestry and Garden Workers\n"
				+ "Food Preparation Assistants\n"
				+ "Labourers\n"
				+ "Military (enlisted)\n"
				+ "Military (officer)\n"
				+ "Retired\n"
				+ "Student\n"
				+ "Home Duties\n"
				+ "Administrative and Support Services\n"
				+ "Arms or Weapons Manufacture or Distribution\n"
				+ "Arts and Recreation Services\n"
				+ "Bar or Licensed Club\n"
				+ "Betting, Bookmaking, Gambling and Gaming\n"
				+ "Cafe and Restaurant\n"
				+ "Charity Community or Social Services\n"
				+ "Construction\n"
				+ "Education and Training\n"
				+ "Electricity, Gas, Water and Waste Services\n"
				+ "Financial and Insurance Services\n"
				+ "Health Care and Social Assistance\n"
				+ "Hotel and Motel\n"
				+ "Information Media and Telecommunications\n"
				+ "Jewel, Gem and Precious Metals\n"
				+ "Manufacturing\n"
				+ "Mining, Gas, Oil and Petroleum\n"
				+ "Money Exchange or Foreign FX Services\n"
				+ "Accommodation and Food Services\n"
				+ "Services\n"
				+ "Professional, Scientific and Technical Services\n"
				+ "Public Administration and Safety\n"
				+ "Rental, Hiring and Real Estate Services\n"
				+ "Retail Trade\n"
				+ "Transport, Postal and Warehousing\n"
				+ "Wholesale Trade\n"
				+ "Farming and Agriculture\n"
				+ "Aviation");
		verifyText(toolbar, "Self-Managed Super Fund");
		verifyText(backButtonText, "Back");
	}

	public void verifyEnteredUserInformation() {
		verifyValueTextFromInputs(ocupationField, "12: Object");
		verifyValueTextFromInputsByNextNumber(inputFields, 1, "Squirrel");
		verifyValueTextFromInputsByNextNumber(inputFields, 2, "7");
		verifyValueTextFromInputsByNextNumber(inputFields, 3, "50,000");
	}
	
	private void progressBarCheck(By step) {
		CompareColorsOfWebElement(step, "rgba(253, 126, 69, 1)");
		print("you are on the right step");
	}

	public void clickNextButton() {
		progressBarCheck(step3);
		progressBarCheck(currentProgressBrick);
		clickOn(nextButton);
		waitForLoaderSpinner();
		progressBarCheck(step4);
	}

	public void clickBackButton() {
		progressBarCheck(step3);
		hoverMouseAndClick(backButton);
		progressBarCheck(step2);
	}

}
