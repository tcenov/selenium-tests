package common;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class A00_RegisterPage extends Base {
	private By registerButtonDisabled = By.cssSelector(".btn.btn-success.btn-block");
	// private By activationLink = By.xpath("//*[contains(text(),
	// 'activate-account')]");
//	private By activationLink = By.cssSelector("a[href*='activate-account']");
	private By mailinatorIframe = By.id("msg_body");
	By activateButton = By.xpath("//*[contains(text(), 'Activate')]");

	protected WebDriver driver;

	public A00_RegisterPage(WebDriver driver) {
		this.driver = driver;
	}

	public void enableAndClickOnRegisterButton() {
		WebElement button = driver.findElement(registerButtonDisabled);
		((JavascriptExecutor) driver).executeScript("arguments[0].removeAttribute('disabled','disabled')", button);
		highlightMyElement(button);
		print("Register button is enabled");
		clickOn(registerButtonDisabled);
		waitForLoaderSpinner();
	}

	public void verifyTextOnPage() {
		verifyText(container,
				"Register\nLet's start your SMSF sign-up\nEmail\nPassword\nPassword should be 8 characters long including uppercase, lowercase letter and number.\nI agree to Terms and Conditions and Privacy Policy\nRegister\nLogin");
		verifyText(toolbar, "Self-Managed Super Fund");
	}

	public void enterPassword(String password) {
		type(passwordField, password);
		print("entered pass " + password);
	}

	public void clickIAgree() {
		clickOn(checkBoxAgree);
		print("clicked on I agree to Terms and Conditions");
	}

	public String generateEmail() {
		// Prepare temporary user's email
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM_dd_hh_mm_ss_SS_aa");
		String email = dateFormat.format(new Date()) + "_test@mailinator.com";
		print("generated email is: " + email);
		return email;
	}

	public void enterEmail(String generatedEmail) {
		type(emailField, generatedEmail);
	}

	public void openEmail(String email) {
		waitForPageLoading();
		open("https://www.mailinator.com");
		By field = By.cssSelector(".form-control");
		type(field, email);
		By go = By.cssSelector(".btn.btn-dark");
		click(go);
		waitForPageLoading();
	}

	public void clickActivationButton() {
		waitForPageLoading();
		suspend(2);
		driver.navigate().refresh();
		suspend(1);
		By subject = By.cssSelector(".all_message-min_text");
		verifyText(subject, "Squirrel Account Activation");
		suspend(1);
		click(subject);
		switchToIframe(mailinatorIframe);
		// open(getText(activationLink));
		clickOn(activateButton);
		switchToBrowserTabByNumber(2);
		// By deleteButton = By.cssSelector(".fa-trash");
		// click(deleteButton);
		waitForPageLoading();
	}

	public void verifyUserActivationText() {
		waitForPageLoading();

		suspend(2);
		verifyText(container, "Activate your account\n" + "We have sent an email to\n" + "\n" + userName + "\n" + "\n"
				+ "Please follow the link to activate your account.\n" + "Send email again\n" + "Register again");
		verifyText(toolbar, "Self-Managed Super Fund");
		// tearDown();
		// driver = new ChromeDriver();
		print("verified user activation page");
		// open(UrlTest);
	}

	public void verifyActivatedUserText() {
		waitForPageLoading();
		verifyText(container, "Activating your account...");
		suspend(2);
		verifyText(container,
				"Thank you!\n" + "Your account is now active. Click the button below to continue\n" + "Login");
		verifyText(toolbar, "Self-Managed Super Fund");
		// tearDown();
		// driver = new ChromeDriver();
		print("verified user activation page");
		// open(UrlTest);
	}

	public void verifyActivationFailedText() {
		waitForPageLoading();
		verifyText(container, "Activating your account...");
		suspend(2);
		verifyText(container,
				"to continue\n" + "Login");
		verifyText(toolbar, "Self-Managed Super Fund");
		// tearDown();
		// driver = new ChromeDriver();
		print("verified user activation page");
		// open(UrlTest);
	}
	
	public void verifyAlreadyRegisteredUserError() {
		verifyPartialText(container, "A user with this email is already registered.");
		print("verified already registered user error ");
	}
}
