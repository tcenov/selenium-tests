package common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class A04_Investment_strategy_page extends Base {
	//client/onboarding/investment_strategy
	private By investmentBlock = By.xpath("//app-investment-strategy");
	protected WebDriver driver;

	public A04_Investment_strategy_page(WebDriver driver) {
		this.driver = driver;
	}

	public void verifyTextOnPage() {
		verifyText(investmentBlock,
				"What do you want to invest in\nYou can change these later if you want to.\nResidential property\nAustralian Shares\nInternational Shares\nTerm deposits and fixed interest\nCryptocurrencies\nCommercial property\nCollectibles\nManaged funds\nCash\nPrecious metals\nOthers\nI'm ready");
		verifyText(toolbar, "Self-Managed Super Fund");
		verifyText(backButtonText, "Back");
	}

	private void progressBarCheck(By step) {
		CompareColorsOfWebElement(step, "rgba(253, 126, 69, 1)");
		print("you are on the right step");
	}

	public void clickNextButton() {
		progressBarCheck(step4);
		progressBarCheck(currentProgressBrick);
		clickOn(nextButton);
		waitForLoaderSpinner();
		progressBarCheck(step5);
	}

	public void clickBackButton() {
		progressBarCheck(step4);
		clickOn(backButton);
		waitForLoaderSpinner();
		progressBarCheck(step3);
	}

}
