package common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class A07_TFN_info_page extends Base {
	// client/onboarding/tfn_info
	protected WebDriver driver;
	By firstFullCheckBox = By.xpath("//label[@for='fund_full']//span");
	By secondPartialCheckBox = By.xpath("//label[@for='fund-1_partial']");
	By thirdNoneCheckBox = By.xpath("//label[@for='fund-2_none']//span");

	public A07_TFN_info_page(WebDriver driver) {
		this.driver = driver;
	}

	public void enterFundName() {
		getFocusAndType(inputs, "My Super s-Peci&l_@");
	}

	public void enterMemberNumber() {
		enterTextInElementByNextNumber(inputs, 2, "7778889990");
	}

	public void selectCheckBoxes() {
		clickOn(firstFullCheckBox);
		// clickOn(secondPartialCheckBox);
		// clickOn(thirdNoneCheckBox);
	}

	public void verifyTextOnPageBeforeSubmit() {
		verifyText(container,
				"Add all your supers\n" + "Fund name\n" + "Member number\n" + "Member number must be 10 digits long.\n"
						+ "How much do you want to trasnfer?\n" + "Full\n" + "Partial\n" + "None\n"
						+ "x Remove this fund\n" + "Add a fund\n" + "Next");

		verifyText(toolbar, "Self-Managed Super Fund");
		verifyText(backButtonText, "Back");
	}

	public void verifyEnteredUserInformationBeforeSubmit() {
		suspend(1);
		testTextFromListOfWebelements(inputFields);
		verifyValueTextFromInputsByNextNumber(inputFields, 1, "My Super s-Peci&l_@");
		verifyValueTextFromInputsByNextNumber(inputFields, 2, "7778889990");
	}

	public void verifyTextOnPageAfterSubmit() {
		verifyText(container,
				"Add all your supers\n" + "My Super s-Peci&l_@\n" + "7778889990\n" + "How much do you want to transfer?\n"
						+ "Full\n" + "Partial\n" + "None\n" + "x Remove this fund\n" + "Fund name\n" + "Member number\n"
						+ "Member number must be 10 digits long.\n" + "How much do you want to trasnfer?\n" + "Full\n"
						+ "Partial\n" + "None\n" + "x Remove this fund\n" + "Add a fund\n" + "Next");

		verifyText(toolbar, "Self-Managed Super Fund");
		verifyText(backButtonText, "Back");
	}

	private void progressBarCheck(By step) {
		CompareColorsOfWebElement(step, "rgba(253, 126, 69, 1)");
		print("you are on the right step");
	}

	public void clickAddFund() {
		clickOnElementByNextNumber(nextButton, 1);
	}

	public void clickNextButton() {
		progressBarCheck(currentProgressBrick);
		if (userName.startsWith("invited_from_")) {
			progressBarCheck(step5);
			clickOnElementByNextNumber(nextButton, 2);
			waitForLoaderSpinner();
			progressBarCheck(step6);
		} else {
			progressBarCheck(step7);
			clickOnElementByNextNumber(nextButton, 2);
			waitForLoaderSpinner();
			progressBarCheck(step8);
		}
	}
		
	public void clickBackButton() {
		progressBarCheck(currentProgressBrick);
		if (userName.startsWith("invited_from_")) {
			progressBarCheck(step5);
			hoverMouseAndClick(backButton);
			waitForLoaderSpinner();
			progressBarCheck(step4);
		} else {
			progressBarCheck(step7);
			hoverMouseAndClick(backButton);
			waitForLoaderSpinner();
			progressBarCheck(step6);
		}
		progressBarCheck(currentProgressBrick);
	}

}
