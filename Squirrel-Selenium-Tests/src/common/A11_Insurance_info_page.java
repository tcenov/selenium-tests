package common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class A11_Insurance_info_page extends Base {
	//client/onboarding/insurance_info
	protected WebDriver driver;

	public A11_Insurance_info_page(WebDriver driver) {
		this.driver = driver;
	}

	public void verifyTextOnPage() {

		verifyText(container, "Choose your insurance\n"
				+ "A$\n"
				+ "Life\n"
				+ "A$\n"
				+ "TPD\n"
				+ "You can always consider insurance later on\nI don't want insurance now\n"
				+ "Next");
		verifyText(toolbar, "Self-Managed Super Fund");
		verifyText(backButtonText, "Back");
	}

	public void selectCheckbox() {
		clickOn(checkBoxAgree);
	}
	
	private void progressBarCheck(By step) {
		CompareColorsOfWebElement(step, "rgba(253, 126, 69, 1)");
		print("you are on the right step");
	}

	public void clickNextButton() {
		progressBarCheck(currentProgressBrick);
		if (userName.startsWith("invited_from_")) {
			progressBarCheck(step8);
			clickOn(nextButton);
			waitForLoaderSpinner();
//			progressBarCheck(step9);
		} else {
			progressBarCheck(step11);
			clickOn(nextButton);
			waitForLoaderSpinner();
//			progressBarCheck(step12);
		}
	}
	
	
	public void clickBackButton() {
		progressBarCheck(step11);
		hoverMouseAndClick(backButton);
		progressBarCheck(currentProgressBrick);
		progressBarCheck(step10);
	}

}
