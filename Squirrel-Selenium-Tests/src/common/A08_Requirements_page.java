package common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class A08_Requirements_page extends Base {
	//client/onboarding/requirements
	protected WebDriver driver;

	public A08_Requirements_page(WebDriver driver) {
		this.driver = driver;
	}

	public void verifyTextOnPage() {
		verifyText(container, "In order to set up an SMSF, there are a few basic requirements you'll need to meet.\n"
				+ "Could you please confirm the following:\n" + "I am not currently Bankrupt\n"
				+ "I am an Australian resident for tax purposes\n" + "I don't have a dishonest criminal record\n"
				+ "I don't have any outstanding debts with the ATO\n" + "I can confirm the above is true\n" + "Next");
		verifyText(toolbar, "Self-Managed Super Fund");
		verifyText(backButtonText, "Back");
	}

	public void selectCheckbox() {
		clickOn(checkBoxAgree);
	}

	private void progressBarCheck(By step) {
		CompareColorsOfWebElement(step, "rgba(253, 126, 69, 1)");
		print("you are on the right step");
	}


	public void clickNextButton() {
		progressBarCheck(currentProgressBrick);
		if (userName.startsWith("invited_from_")) {
			progressBarCheck(step6);
			clickOn(nextButton);
			waitForLoaderSpinner();
			progressBarCheck(step7);
		} else {
			progressBarCheck(step8);
			clickOn(nextButton);
			waitForLoaderSpinner();
			progressBarCheck(step9);
		}
		progressBarCheck(currentProgressBrick);
	}
	
	public void clickBackButton() {
		progressBarCheck(currentProgressBrick);
		if (userName.startsWith("invited_from_")) {
			progressBarCheck(step6);
			hoverMouseAndClick(backButton);
			waitForLoaderSpinner();
			progressBarCheck(step5);
		} else {
			progressBarCheck(step8);
			hoverMouseAndClick(backButton);
			waitForLoaderSpinner();
			progressBarCheck(step7);
		}
		progressBarCheck(currentProgressBrick);
	}
	
	
}
