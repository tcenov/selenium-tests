package common;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EmptyStackException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import com.google.common.base.Function;

public class Base extends Maps {

	By emailField = By.xpath("//input[@type='email']");
	By passwordField = By.xpath("//input[@type='password']");

	protected static WebDriver driver;
	protected int timeOut = 40;
	private int pageLoadTimeOut = 40;
	private static String testEnvironment = "dev"; // could be "dev", "test" or "stage"
	private static String UrlDev = "http://dev.squirrelsuper.com.au/client/";
	private static String UrlStage = "http://stag.squirrelsuper.com.au/client/";
	protected static String UrlTest = "http://testing.squirrelsuper.com.au/client/";
	protected String userName = "testa164@mailinator.com"; // invited_from_
	protected String userPassword = "Tarator1";

	@BeforeClass
	public static void setUp() throws AWTException, InterruptedException, UnsupportedEncodingException {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		
//		Thread.sleep(1000);
//		Robot robot = new Robot();
//		robot.keyPress(KeyEvent.VK_WINDOWS);
//		robot.keyPress(KeyEvent.VK_SHIFT);
//		robot.keyPress(KeyEvent.VK_RIGHT);
//		robot.keyRelease(KeyEvent.VK_RIGHT);
//		robot.keyRelease(KeyEvent.VK_SHIFT);
//		robot.keyRelease(KeyEvent.VK_WINDOWS);
//		print("moved browser to next monitor");
		
		// WebDriverWait wait = new WebDriverWait(driver, 10);
		// Alert alert = wait.until(ExpectedConditions.alertIsPresent());
		
		driver.manage().window().setPosition(new Point(2000,1));
		driver.manage().window().maximize();
		
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		switch (testEnvironment) {
		case "dev":
			driver.get(UrlDev);
			break;
		case "test":
			driver.get(UrlTest);
			break;
		case "stage":
			driver.get(UrlStage);
			break;
		}
		
		print("-Started-new-test--------------------------------------------");
	}

	private void authenticate(String username, String password) throws AWTException, InterruptedException {

		Robot robot = new Robot();
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable transferable = new StringSelection(username);
		clipboard.setContents(transferable, null);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		Thread.sleep(50);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);

		robot.keyPress(KeyEvent.VK_TAB);

		transferable = new StringSelection(password);
		clipboard.setContents(transferable, null);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		Thread.sleep(50);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);

		robot.keyPress(KeyEvent.VK_ENTER);
		print("Authenticated");
	}
	
	private void moveBrowserToSecondMonitor() throws AWTException {

		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_WINDOWS);
		robot.keyPress(KeyEvent.VK_SHIFT);
		robot.keyPress(KeyEvent.VK_RIGHT);
		
		robot.keyRelease(KeyEvent.VK_RIGHT);
		robot.keyRelease(KeyEvent.VK_SHIFT);
		robot.keyRelease(KeyEvent.VK_WINDOWS);
		print("moved browser to next monitor");
	}

	// @After
	private void cleanUp() {
		driver.manage().deleteAllCookies();
	}

	@AfterClass
	public static void tearDown() {
		driver.manage().deleteAllCookies();
		driver.close();
		try {
			driver.close();
			// Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("Taskkill /F /IM chromedriver.exe");
		} catch (Exception anException) {
			// anException.printStackTrace();
		}
	}

	protected void open(String url) {
		driver.get(url);
		waitForPageToLoad();
		driver.manage().window().maximize();
	}

	public void openStepByNumber(int stepNumber) {
		final String envUrl;
		if (testEnvironment.equals("dev")) {
			envUrl = UrlDev;

		} else {
			envUrl = UrlTest;
		}

		switch (stepNumber) {
		case 1:
			open(envUrl + "basic_info");
			break;
		case 2:
			open(envUrl + "additional_info");
			break;
		case 3:
			open(envUrl + "onboarding/financial_info");
			break;
		case 4:
			open(envUrl + "onboarding/investment_strategy");
			break;
		case 5:
			open(envUrl + "onboarding/fund_info");
			break;
		case 6:
			open(envUrl + "onboarding/identity_info");
			break;
		case 7:
			open(envUrl + "onboarding/tfn_info");
			break;
		case 8:
			open(envUrl + "onboarding/requirements");
			break;
		case 9:
			open(envUrl + "onboarding/fees_info");
			break;
		case 10:
			open(envUrl + "onboarding/payment_info");
			break;
		case 11:
			open(envUrl + "onboarding/insurance_info");
			break;
		case 12:
			open(envUrl + "onboarding/almost_done");
			break;
		case 13:
			open(envUrl + "onboarding/complete");
			break;
		case 14:
			open("http://ec2-54-153-249-209.ap-southeast-2.compute.amazonaws.com/privacy_policy");
			break;
		case 15:
			open("http://ec2-54-153-249-209.ap-southeast-2.compute.amazonaws.com/terms_conditions");
			break;
		}
	}

	protected void waitForLoaderSpinner() {

		WebElement loader = driver.findElement(loaderSpinner);
		WebDriverWait wait = new WebDriverWait(driver, timeOut);
		wait.until(ExpectedConditions.visibilityOf(loader));
		print("Loading... ");
		wait.until(ExpectedConditions.invisibilityOf(loader));
		suspend(1);
		print("Loading finished, data is ready");
	}

	protected void waitForAjax() throws InterruptedException {

		while (true) {
			// Boolean ajaxIsComplete = (Boolean)
			// ((JavascriptExecutor)driver).executeScript("return jQuery.active
			// == 0");
			Boolean ajaxIsComplete = (Boolean) ((JavascriptExecutor) driver)
					.executeScript("return (window.jQuery != null) && (jQuery.active === 0);");
			if (ajaxIsComplete) {
				break;
			}
			Thread.sleep(100);
		}
	}

	private void waitForAjaxRequestFinish(final int timeout, final String jsQuery) {
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		Function<WebDriver, Boolean> function = new Function<WebDriver, Boolean>() {
			public Boolean apply(WebDriver input) {
				Boolean condition = false;
				Object activeRequests = ((JavascriptExecutor) driver).executeScript(jsQuery);
				if (activeRequests instanceof Long) {
					Long n = (Long) activeRequests;
					if (n.longValue() == 0L) {
						condition = true;
					}
				} else {
					patchXMLHttpRequest(timeout);
				}
				return condition;
			}
		};
		try {
			wait.until(function);
		} catch (TimeoutException e) {
		}
	}

	public void uploadFile(By selector, CharSequence... fileLocation) throws InterruptedException {
		WebElement fileInput = driver.findElement(selector);
		Thread.sleep(1000);
		fileInput.sendKeys(fileLocation);
		Thread.sleep(1000);
		print("uploadFile finished ");
	}

	private void patchXMLHttpRequest(int timeout) {
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		Function<WebDriver, Boolean> function = new Function<WebDriver, Boolean>() {
			public Boolean apply(WebDriver input) {
				Boolean condition = false;
				Object activeRequests = ((JavascriptExecutor) driver).executeScript("return window.openHTTPs");
				if (activeRequests instanceof Long) {
					Long n = (Long) activeRequests;
					if (n.longValue() > 0L) {
						condition = true;
					}
				}
				return condition;
			}
		};
		try {
			if (driver instanceof JavascriptExecutor) {
				JavascriptExecutor jsDriver = (JavascriptExecutor) driver;
				Object activeRequests = jsDriver.executeScript("return window.openHTTPs");
				if (activeRequests instanceof Long) {
					return;
				}
				String script = "  (function() {" + "var oldOpen = XMLHttpRequest.prototype.open;"
						+ "window.openHTTPs = 0;"
						+ "XMLHttpRequest.prototype.open = function(method, url, async, user, pass) {"
						+ "window.openHTTPs++;" + "this.addEventListener('readystatechange', function() {"
						+ "if(this.readyState == 4) {" + "window.openHTTPs--;" + "}" + "}, false);"
						+ "oldOpen.call(this, method, url, async, user, pass);" + "}" + "})();";
				jsDriver.executeScript(script);
				// wait for default state of 0 change to 1, because on fast
				// connections the method fail because execute
				// the query before the request is launched for the page
				wait.until(function);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void openEmail(String generatedEmail) {
		waitForPageLoading();
		open("https://www.mailinator.com");
		By field = By.cssSelector(".form-control");
		type(field, generatedEmail);
		By go = By.cssSelector(".btn.btn-dark");
		click(go);
		waitForPageLoading();
	}

	private void verifyInvitationEmail() {
		waitForPageLoading();
		driver.navigate().refresh();
		// assertText(sender, "info@fff.com");

		By subject = By.cssSelector(".all_message-min_text");
		assertTexts(subject, "Your fiend invited you to ????? !", "test ");

		click(subject);

		suspend(1);
		By deleteButton = By.cssSelector(".fa-trash");
		click(deleteButton);

		waitForPageLoading();
	}

	private void clickButton(By selector) {
		waitForPageLoading();
		if (isElementPresent(selector)) {
			print("item is present - " + selector);
		} else {
			print("item is not present - " + selector);
		}

		if (isClickable(selector)) {
			suspend(1);
			print("item is clickable - " + selector);
			click(selector);
		} else {
			print("item is not clickable - " + selector);
			throw new EmptyStackException();
		}
		print("clicked button " + selector);
	}

	private void agreeWithTermsAndConditions() {

		WebElement element = driver.findElement(checkBoxAgree);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
		print("clicked agreeWithTermsAndConditions");
		waitForPageLoading();
	}

	protected void hoverToElement(By selector) {

		try {
			highlightMyElement(selector);
			Actions builder = new Actions(driver);
			builder.moveToElement(driver.findElement(selector)).build().perform();
		} catch (NoSuchElementException e) {

		}
	}

	protected Select select(By selector) {
		highlightMyElement(selector);
		return new Select(driver.findElement(selector));
	}

	protected void findElements(By selector) {

		try {
			WebDriverWait wait = new WebDriverWait(driver, timeOut);
			wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
			highlightMyElement(selector);
		} catch (Exception e) {

			throw e;
		}
	}

	// This is just a helpful method and can be deleted.
	// deletion will not affect anything
	protected void testWebElement(By selector) {
		// WebElement element = driver.findElement(selector);
		print("testWebElement started ----------------------------------");
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		// String actualText = driver.findElement(selector).getText();
		print(" value = " + driver.findElement(selector).getAttribute("value"));
		print(" text = " + driver.findElement(selector).getText());
		print(" tag = " + driver.findElement(selector).getTagName());
		print(" item-id = " + driver.findElement(selector).getAttribute("item-id"));
		print(" placeholder = " + driver.findElement(selector).getAttribute("placeholder"));
		print(" style = " + driver.findElement(selector).getAttribute("style"));
		if (isClickable(selector)) {
			print("item is clickable - " + selector);
		} else
			print("item is not clickable - " + selector);

		if (isElementPresent(selector)) {
			print("item is present - " + selector);
		} else {
			print("item is not present - " + selector);
		}

		highlightMyElement(selector);

		// Assert.assertEquals(actualText, text);
		// print("Assert finished. " + "ActualText is: " + actualText);
		print("testWebElement finished ---------------------------------");
	}

	// This is just a helpful method and can be deleted.
	// deletion will not affect anything
	protected void testWebElement(WebElement element) {
		print("testWebElement started ----------------------------------");
		print(" value = " + element.getAttribute("value"));
		print(" text = " + element.getText());
		print(" tag = " + element.getTagName());
		print(" item-id = " + element.getAttribute("item-id"));
		print(" placeholder = " + element.getAttribute("placeholder"));
		print(" style = " + element.getAttribute("style"));
		print(" href = " + element.getAttribute("href"));
		print("testWebElement finished ---------------------------------");
	}

	// This is just a helpful method and can be deleted.
	// deletion will not affect anything
	protected void testTextFromListOfWebelements(By selector) {
		WebElement element;
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		List<WebElement> list = driver.findElements(selector);
		print("----------------------print all element's positions with extracted text-----------------");
		print("list size is :" + list.size());
		for (int i = 0; i < list.size(); i++) {
			element = list.get(i);
			// if (element.getText().isEmpty()) {
			// continue;
			// }
			print("Position = " + i + " Text = " + element.getText() + " Value = " + element.getAttribute("value")
					+ " Tag = " + element.getTagName() + " placeholder = "
					+ driver.findElement(selector).getAttribute("placeholder"));
			// print(" item-id = " +
			// driver.findElement(selector).getAttribute("item-id"));
			// print(" style = " +
			// driver.findElement(selector).getAttribute("style"));
			// print(" text = " + element.getText());
			// print(" tag = " + element.getTagName());
			// print(" value = " + element.getAttribute("value"));
		}
		print("----------------------printed all elements and positions with extracted text-----------------");
	}

	protected void testLinkElements() {
		print("testLinkElements started ----------------------------------");
		// get all the <a> elements
		List<WebElement> linkList = driver.findElements(By.tagName("a"));

		for (int i = 0; i < linkList.size(); i++) {
			WebElement element = linkList.get(i);
			testWebElement(element);
		}
		print("testLinkElements finished ---------------------------------");
	}

	protected void testColorOfWebElement(By selector) {
		print("testWebElement started ----------------------------------");
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		print(" color = " + driver.findElement(selector).getCssValue("color"));
		print(" background-color = " + driver.findElement(selector).getCssValue("background-color"));

		// highlightMyElement(selector);
		print("testWebElement finished ---------------------------------");
	}

	protected String getColorOfWebElement(By selector) {
		String color = driver.findElement(selector).getCssValue("background-color");
		return color;
	}

	protected void CompareColorsOfWebElement(By selector, String colorCode) {
		print("compare colors");
		String color = getColorOfWebElement(selector);
		print("color is: " + color);
		assert color.equals("rgba(253, 126, 69, 1)");

	}

	// verify placeholder's text from inputs
	protected void verifyPlaceholdersTextFromInputs(By selector, String text) {
		waitForPageLoading();
		String actualText = driver.findElement(selector).getAttribute("placeholder");
		highlightMyElement(selector);
		Assert.assertEquals(actualText, text);
		print("Assert finished. " + "ActualText is: " + actualText);
	}

	// verify placeholder's text from inputs
	protected void verifyPlaceholdersTextFromInputsByNextNumber(By selector, int nextNumber, String text) {
		waitForPageLoading();
		List<WebElement> elements = driver.findElements(selector);
		print("elements list = " + elements.size());

		if (elements.isEmpty()) {
			throw new IllegalStateException("there are no more elements!");
		} else {
			highlightMyElement(elements.get(nextNumber - 1));

			testTextFromListOfWebelements(selector);

			String actualText = elements.get(nextNumber - 1).getAttribute("placeholder");
			Assert.assertEquals(actualText, text);
			print("Assert finished. " + "ActualText is: " + actualText);
		}
	}

	// verify value text from inputs
	protected void verifyValueTextFromInputsByNextNumber(By selector, int nextNumber, String text) {
		waitForPageLoading();
		List<WebElement> elements = driver.findElements(selector);
		print("elements list = " + elements.size());

		if (elements.isEmpty()) {
			throw new IllegalStateException("there are no more elements!");
		} else {
			highlightMyElement(elements.get(nextNumber - 1));
			String actualText = elements.get(nextNumber - 1).getAttribute("value");
			Assert.assertEquals(actualText, text);
			print("Assert finished. " + "ActualText is: " + actualText);
		}
	}

	// verify text from elements
	protected void verifyTextFromElementsByNextNumber(By selector, int nextNumber, String text) {
		waitForPageLoading();
		List<WebElement> elements = driver.findElements(selector);
		print("elements list = " + elements.size());

		if (elements.isEmpty()) {
			throw new IllegalStateException("there are no more elements!");
		} else {
			highlightMyElement(elements.get(nextNumber - 1));
			String actualText = elements.get(nextNumber - 1).getText();
			Assert.assertEquals(actualText, text);
			print("Assert finished. " + "ActualText is: " + actualText);
		}
	}

	// verify value's text from inputs
	protected void verifyValueTextFromInputs(By selector, String text) {

		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		String actualText = driver.findElement(selector).getAttribute("value");
		highlightMyElement(selector);
		Assert.assertEquals(actualText, text);
		print("Assert finished. " + "ActualText is: " + actualText);
	}

	// verify label's text from inputs
	protected void verifyLabelTextFromInputs(By selector, String text) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		String actualText = driver.findElement(selector).getAttribute("label");
		highlightMyElement(selector);
		Assert.assertEquals(actualText, text);
		print("Assert finished. " + "ActualText is: " + actualText);
	}

	// verify text from selector
	protected void verifyText(By selector, String text) {
		waitForPageLoading();
		waitForTextTobePresent(selector, text);
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
		String actualText = driver.findElement(selector).getText();
		highlightMyElement(selector);
		Assert.assertEquals(actualText, text);
		print("Assert finished. " + "ActualText is: " + actualText);
	}

	protected void verifyPartialText(By selector, String expectedText) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		String actualText = driver.findElement(selector).getText();
		Assert.assertTrue(actualText.contains(expectedText));
		highlightMyElement(selector);
		print("Assert finished. " + "verified text: " + expectedText);
	}

	protected void clickOn(By selector) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		// wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
		wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		wait.until(ExpectedConditions.elementToBeClickable(selector));
		WebElement element = driver.findElement(selector);
		Boolean isPresent = driver.findElements(selector).size() > 0;
		// boolean isVisible = driver.findElement(selector).isDisplayed();
		if (isPresent) {
			highlightMyElement(selector);
			// driver.findElement(selector).click();
			Actions actions = new Actions(driver);
			actions.moveToElement(element).click().perform();
			print("clicked on " + selector);
			waitForPageLoading();
		} else {
			suspend(1);
			highlightMyElement(selector);
			// driver.findElement(selector).click();
			Actions actions = new Actions(driver);
			actions.moveToElement(element).click().perform();
			print("clicked on " + selector);
			waitForPageLoading();
		}
	}

	protected void click(By selector) {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		wait.until(ExpectedConditions.elementToBeClickable(selector));
		WebElement element = driver.findElement(selector);
		Boolean isPresent = driver.findElements(selector).size() > 0;
		// boolean isVisible = driver.findElement(selector).isDisplayed();
		if (isPresent) {
			highlightMyElement(selector);
			driver.findElement(selector).click();
			print("clicked on " + selector);
			waitForPageLoading();
		} else {
			suspend(1);
			highlightMyElement(selector);
			// driver.findElement(selector).click();
			Actions actions = new Actions(driver);
			actions.moveToElement(element).click().perform();
			print("clicked on " + selector);
			waitForPageLoading();
		}
	}

	protected void hoverMouseAndClick(By selector) {
		WebElement element = driver.findElement(selector);
		Actions actions = new Actions(driver);
		actions.moveToElement(element).click().perform();
		print("clicked on " + selector);
	}

	protected void hoverMouseAndClickOnLink(By partialLink) {
		WebElement element = driver.findElement(partialLink);
		Actions actions = new Actions(driver);
		actions.moveToElement(element).click().perform();
		print("clicked on " + partialLink);
	}

	private void acceptAlert(String expectedAlertMessage) {
		String actualAlertMessage = driver.switchTo().alert().getText();
		print(actualAlertMessage);
		Boolean continueTest = false;
		continueTest = actualAlertMessage.equals(expectedAlertMessage);
		if (continueTest == true) {
			driver.switchTo().alert().accept();
		} else {
			String url = driver.getCurrentUrl();
			print(url);
			throw new IllegalStateException("actualAlertMessage is: " + actualAlertMessage);
		}
	}

	protected String getText(By selector) {
		highlightMyElement(selector);
		String text = driver.findElement(selector).getText();
		return text;
	}

	protected void navigateBack() {
		driver.navigate().back();
		print("Navigated back");
	}

	protected void clickOnElementByNextNumber(By selector, int nextNumber) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(selector));

		List<WebElement> elements = driver.findElements(selector);
		print("elements list = " + elements.size());

		if (elements.isEmpty()) {
			throw new IllegalStateException("there are no more elements!");
		} else {
			highlightMyElement(elements.get(nextNumber - 1));
			// elements.get(nextNumber - 1).click();
			Actions actions = new Actions(driver);
			actions.moveToElement(elements.get(nextNumber - 1)).click().perform();
			print("clicked on element" + nextNumber);
		}
	}

	protected void enterTextInElementByNextNumber(By selector, int nextNumber, String text) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		// wait.until(ExpectedConditions.elementToBeSelected(selector));

		List<WebElement> elements = driver.findElements(selector);
		print("elements list = " + elements.size());

		if (elements.isEmpty()) {
			throw new IllegalStateException("there are no more elements!");
		} else {
			highlightMyElement(elements.get(nextNumber - 1));
			// elements.get(nextNumber - 1).sendKeys(text);
			new Actions(driver).moveToElement(elements.get(nextNumber - 1)).click().sendKeys(text).perform();
			print("entered " + text + " on element number " + nextNumber);

		}
	}

	private void clear(By selector) {
		try {
			highlightMyElement(selector);
			driver.findElement((selector)).clear();
		} catch (NoSuchElementException e) {

		}
	}

	protected void type(By selector, String text) {
		try {
			findElements(selector);
			highlightMyElement(selector);
			driver.findElement(selector).clear();
			driver.findElement(selector).sendKeys(text);
			print("Entered: " + text);
		} catch (NoSuchElementException e) {

		}
	}

	protected void getFocusAndType(By selector, String text) {
		WebElement element = driver.findElement(selector);
		highlightMyElement(selector);
		// clear(selector);
		new Actions(driver).moveToElement(element).click().sendKeys(text).perform();
		print("Entered: " + text);
	}

	protected boolean isElementPresent(By selector) {

		boolean found = true;
		try {

			WebDriverWait wait = new WebDriverWait(driver, timeOut);
			wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
		} catch (NoSuchElementException | TimeoutException | ScreenshotException e) {
			found = false;
		}
		return found;
	}

	protected boolean waitTillIframeIsVisible(String iframe) {

		boolean found = true;
		try {

			WebDriverWait wait = new WebDriverWait(driver, timeOut);
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(iframe));
		} catch (NoSuchElementException | TimeoutException | ScreenshotException e) {
			found = false;
		}
		return found;
	}

	protected void selectNewOpenedWindow() {

		try {

			for (String newWindow : driver.getWindowHandles()) {
				print("newWindow = " + newWindow);
				driver.switchTo().window(newWindow);
				print("switched to the new tab");
			}
		} catch (Exception e) {
			System.out.println("The requested window is not valid");
		}
	}

	protected boolean isCheckBoxChecked(String checkBoxName) {

		boolean checked = (boolean) ((JavascriptExecutor) driver).executeScript(
				" function isChecked(searchedItem) { var surveyinnerId = document.getElementById('surveysListsInnerContainer'),labels = surveyinnerId.getElementsByTagName('label'),checked = [];for (var i = 0; i < labels.length; i++) { var input = labels[i].getElementsByTagName('input'), span = labels[i].getElementsByTagName('span'); input[0].checked && span[0].innerHTML == searchedItem ? checked.push(input[i]) : false; }return checked.length != 0;} return isChecked('"
						+ checkBoxName + "');");

		return checked;
	}

	protected boolean isClickable(By selector) {

		WebDriverWait wait = new WebDriverWait(driver, timeOut);

		try {
			wait.until(ExpectedConditions.elementToBeClickable(selector));
			System.out.println("item " + selector + " is " + true);
			return true;
		} catch (TimeoutException e) {
			return false;
		}
	}

	protected boolean isSelected(By selector, String value) {

		org.openqa.selenium.support.ui.Select dropDown = select(selector);
		String selectedValue = dropDown.getFirstSelectedOption().getText();
		if (selectedValue.equals(value)) {
			return true;
		} else {
			return false;
		}

	}

	protected void switchToIframe(By selector) {

		try {

			driver.switchTo().frame(driver.findElement(selector));

		} catch (NoSuchFrameException e) {

		}
	}

	protected void navigate(String url) {
		driver.navigate().to(url);

	}

	protected void switchToTopWindow() {
		driver.switchTo().defaultContent();
		print("switched to default iFrame");
	}

	protected void switchToBrowserTabByNumber(int tabNumber) {
		ArrayList<String> browserTabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(browserTabs.get(tabNumber - 1));
		print("switched to tab number: " + tabNumber);
		waitForPageLoading();
	}

	protected void waitForPageToLoad() {
		driver.manage().timeouts().pageLoadTimeout(pageLoadTimeOut, TimeUnit.SECONDS);

	}

	protected void waitForPageLoading() {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		});
		print("Page is loaded.");
	}

	protected void stopSelenium() {
		try {
			driver.close();
			// Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe");
			Runtime.getRuntime().exec("Taskkill /F /IM chromedriver.exe");
		} catch (Exception anException) {
			// anException.printStackTrace();
		}
	}

	protected String getTimestamp() {

		return (Long.toString(new Date().getTime() / 1000));
	}

	protected boolean isTextPresent(By selector, String text) {

		try {

			if (driver.findElement(selector).getText().contains(text))
				;

			return true;

		} catch (NullPointerException e) {

			return false;
		}
	}

	private boolean waitForTextTobePresent(By selector, String text) {
		boolean found = true;
		try {

			WebDriverWait wait = new WebDriverWait(driver, timeOut);
			wait.until(ExpectedConditions.textToBePresentInElement(driver.findElement(selector), text));

		} catch (TimeoutException e) {
			e.printStackTrace();
			found = false;
		}
		return found;
	}

	protected void close() {

		driver.close();
		try {
			Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void pressKey(By selector, Keys key) {

		try {
			findElements(selector);
			driver.findElement(selector).sendKeys(key);
		} catch (NoSuchElementException e) {

		}
	}

	public void takeScreenShotOnFailure(String fileName) throws IOException {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		DateFormat dateFormat = new SimpleDateFormat("hh_mm_ss_SS_aa");
		String destDir = "screenshots/Failures";
		new File(destDir).mkdirs();
		String destFile = fileName + "_" + dateFormat.format(new Date()) + ".png";
		FileUtils.copyFile(scrFile, new File(destDir + "/" + destFile));
	}

	private void assertText(By selector, String expectedText) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		// wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		String actualText = driver.findElement(selector).getText();
		highlightMyElement(selector);
		Assert.assertEquals(actualText, expectedText);
		print("Assert finished. " + "ActualText is: " + actualText);
	}

	private void assertTexts(By selector, String expectedText, String expectedText2) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		// wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		String actualText = driver.findElement(selector).getText();
		highlightMyElement(selector);
		assert actualText.equals(expectedText) || actualText.equals(expectedText2);
		print("Assert finished. " + "ActualText is: " + actualText);
	}

	private void assertTexts(By selector, String expectedText, String expectedText2, String expectedText3,
			String expectedText4) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		// wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		String actualText = driver.findElement(selector).getText();
		highlightMyElement(selector);
		assert actualText.equals(expectedText) || actualText.equals(expectedText2) || actualText.equals(expectedText3)
				|| actualText.equals(expectedText4);
		print("Assert finished. " + "ActualText is: " + actualText);
	}

	private void assertTextsFromListOfElements(By selector, String expectedText) {
		WebDriverWait wait = new WebDriverWait(driver, 150);
		wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		List<WebElement> elements = driver.findElements(selector);
		for (int i = 0; i < elements.size(); i++) {
			String actualText = elements.get(i).getText();
			if (new String(actualText).equals(expectedText)) {
				Assert.assertEquals(actualText, expectedText);
				print("Assert finished. " + "ActualText is: " + actualText);
				highlightMyElement(elements.get(i));
				return;
			} else {
				// print("Continue, attempt " + (i+1));
			}
		}
		throw new IllegalStateException("assertTextsFromListOfElements() failed!" + "didn't found " + expectedText);
	}

	protected void highlightMyElement(By selector) {
		WebElement element = driver.findElement(selector);
		JavascriptExecutor javascript = (JavascriptExecutor) driver;
		javascript.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
				"color: orange; border: 4px solid orange;");
		print("Highlighted " + selector);
	}

	protected void highlightMyElement(WebElement element) {
		JavascriptExecutor javascript = (JavascriptExecutor) driver;
		javascript.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
				"color: orange; border: 4px solid orange;");
		print("Highlighted element");
	}

	public void suspend(int seconds) {
		try {
			print("Start waiting for " + seconds + " seconds.");
			for (int i = 1; i <= seconds; i++) {
				print("Wait " + i + " seconds.");
				Thread.sleep(1000);
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	protected static void print(String text) {
		System.out.println(text);
	}

}
