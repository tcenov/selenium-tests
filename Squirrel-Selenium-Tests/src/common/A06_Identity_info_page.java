package common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class A06_Identity_info_page extends Base {
	// client/onboarding/identity_info
	private By uploadFrontSide = By.cssSelector(".inputfile");
	private By uploadBackSide = By.id("in_id_back");
	private By inputs = By.cssSelector(".input-container");
	private By calendarCells = By.cssSelector(".mat-calendar-body-cell-content");
	private By nextButton = By.cssSelector(".btn-success");
	private By documents = By.cssSelector(".content.valid");
	protected WebDriver driver;

	public A06_Identity_info_page(WebDriver driver) {
		this.driver = driver;
	}

	public void verifyTextOnPageAndFirstPicture() {

		verifyText(container, "Upload your ID.\n" + "File requirements\n" + "Front side of ID\n" + "Update\n"
				+ "Make sure the information on the attachment is readable.\n" + "Back side of ID\n" + "Update\n"
				+ "Attachment should only be on white background.\n" + "ID expiration date\n" + "ID number\n" + "Next");
		verifyText(toolbar, "Self-Managed Super Fund");
		verifyText(backButtonText, "Back");
		testWebElement(documents);
	}

	public void verifyEnteredUserInformation() {
		suspend(1);
		verifyValueTextFromInputsByNextNumber(inputFields, 3, "December 30, 2031");
		verifyValueTextFromInputsByNextNumber(inputFields, 4, "123 456 789");
	}

	public void uploadDocuments() throws InterruptedException {
		uploadFile(uploadFrontSide, "D:\\cow.jpg");
		uploadFile(uploadBackSide, "D:\\cow1.jpg");
	}

	public void selectIDExpirationDate() {
		clickOnElementByNextNumber(inputs, 1);
		suspend(1);
		clickOnElementByNextNumber(calendarCells, 16);
		clickOnElementByNextNumber(calendarCells, 12);
		clickOnElementByNextNumber(calendarCells, 31);
		verifyValueTextFromInputsByNextNumber(inputFields, 3, "December 31, 2031");
	}

	public void enterIDNumber() {
		enterTextInElementByNextNumber(inputs, 2, "123456789");
	}

	private void progressBarCheck(By step) {
		CompareColorsOfWebElement(step, "rgba(253, 126, 69, 1)");
		print("you are on the right step");
	}

	public void clickNextButton() {
		progressBarCheck(currentProgressBrick);
		if (userName.startsWith("invited_from_")) {
			progressBarCheck(step4);
			clickOn(nextButton);
			waitForLoaderSpinner();
			progressBarCheck(step5);
		} else {
			progressBarCheck(step6);
			clickOn(nextButton);
			waitForLoaderSpinner();
			progressBarCheck(step7);
		}
	}

	public void clickBackButton() {
		progressBarCheck(step6);
		suspend(2);
		hoverMouseAndClick(backButton);
		progressBarCheck(step5);
	}

}
