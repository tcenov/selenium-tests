package common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class A00_HomePage extends Base {
	private By termsAndConditionLink = By.xpath("//a[contains(text(),'Terms and Conditions')]");
	private By privacyPoliceLink = By.xpath("//a[contains(text(),'Privacy Policy')]");
	private By loginButton = By.xpath("//button[@class='btn btn-success btn-block btn-next no-margin-top']");
	private By forgotPasswordLink = By.cssSelector(".text-success");
	protected WebDriver driver;

	public A00_HomePage(WebDriver driver) {
		this.driver = driver;
	}

	public void verifyTextOnPage() {
		verifyText(container,
				"Login\nWelcome back.\nEmail\nPassword\nRemember me\nLogin\nI forgot my password\nRegister");
		verifyText(toolbar, "Self-Managed Super Fund");
	}

	public void enterEmail(String email) {
		type(emailField, email);
		print("entered email " + email);
	}

	public void enterPassword(String password) {
		type(passwordField, password);
		print("entered pass " + password);
	}

	public void clickLogInButton() {
		waitForPageLoading();
		findElements(loginButton);
		clickOn(loginButton);
		print("clicked Log In!!!!");
		waitForLoaderSpinner();
		waitForPageLoading();
	}

	public void clickRegisterButton() {
		clickOn(registerButton);
		waitForPageToLoad();
		print("Registration is loaded");
	}

	public void clickResetPasswordLink() {
		clickOn(forgotPasswordLink);
		waitForPageToLoad();
		print("Registration is loaded");
		verifyText(container, "Reset password\nPlease enter the email you used to register\nEmail\nReset\nI remembered!");
	}

	public void clickTermsAndConditionLink() {
		clickOn(termsAndConditionLink);
	}

	public void clickPrivacyPoliceLink() {
		clickOn(privacyPoliceLink);
	}
}
