package common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class A05_Fund_info_page extends Base {
	// client/onboarding/fund_info
	private By member = By.tagName("li");
	private By fundNameField = By.xpath("//input[@type='text']");
	private By mailinatorIframe = By.id("msg_body");
	private By mailinatorEmail = By.tagName("tr");
	protected WebDriver driver;

	public A05_Fund_info_page(WebDriver driver) {
		this.driver = driver;
	}

	public void enterFundName() {
		getFocusAndType(fundNameField, userName.replaceAll("@", "") + ".FundName");
		suspend(2);
	}

	public void inviteMember() {
		clickOnElementByNextNumber(member, 1);
		enterTextInElementByNextNumber(inputs, 2, "invited_from_" + userName);
	}

	public void selectCheckbox() {
		clickOn(checkBoxAgree);
	}

	public void verifyTextOnPage() {
		verifyPartialText(container, "Name your fund");
		verifyPartialText(container, "Fund name");
		verifyPartialText(container, "Fund names can be anything you like. So you could have");
		verifyPartialText(container, "Your Name Lifetime Superfund");
		verifyPartialText(container, "or something fun like");
		verifyPartialText(container, "On The Beach Lifetime Superfund");
		verifyPartialText(container, "Do you want to have any additional members in your SMSF?");
		verifyPartialText(container, "Let us know how many.");
		verifyPartialText(container, "Number of members you will invite");
		verifyPartialText(container, "123");
		verifyPartialText(container, "I don't want to invite more people");
		verifyPartialText(container, "Next");
		verifyText(toolbar, "Self-Managed Super Fund");
		verifyText(backButtonText, "Back");
	}

	public void verifyEnteredUserInformation() {
		suspend(1);
		testTextFromListOfWebelements(inputFields);
		verifyValueTextFromInputs(inputFields, userName.replaceAll("@", "") + ".FundName");
	}

	public void verifyInvitedEmail(String invitedEmail) {
		waitForPageLoading();
		open("https://www.mailinator.com");
		By field = By.cssSelector(".form-control");
		type(field, invitedEmail);
		By go = By.cssSelector(".btn.btn-dark");
		click(go);
		waitForPageLoading();
		
		suspend(1);
		driver.navigate().refresh();
		suspend(1);
		By subject = By.cssSelector(".all_message-min_text");
		verifyText(subject, "Squirrel Invitation");
		suspend(1);
		click(subject);
		switchToIframe(mailinatorIframe);
		testTextFromListOfWebelements(mailinatorEmail);
		verifyTextFromElementsByNextNumber(mailinatorEmail, 4, "Hello");
		verifyTextFromElementsByNextNumber(mailinatorEmail, 6, "Ivan-Asen Last has created a Self-Managed-Super-Fund with Squirrel and is inviting you to join! To get onboarded, follow the link below:");
		verifyTextFromElementsByNextNumber(mailinatorEmail, 8, "Get onboard");
		verifyTextFromElementsByNextNumber(mailinatorEmail, 9, "Get onboard");
		verifyTextFromElementsByNextNumber(mailinatorEmail,11, "It's fast and easy but if you face any difficulty, do not hesitate to get in touch with us:");
		verifyTextFromElementsByNextNumber(mailinatorEmail,12 , "136 887 (8:30 am - 5:30 pm weekdays)");
		verifyTextFromElementsByNextNumber(mailinatorEmail, 13, "+61 2 8823 7999 (from outside Australia)");
		verifyTextFromElementsByNextNumber(mailinatorEmail,14 ,"https://www.squirrelsuper.com.au/contact/" );
		verifyTextFromElementsByNextNumber(mailinatorEmail, 16, "Cheers,");
		verifyTextFromElementsByNextNumber(mailinatorEmail, 18, "The Squirrel Team");
		
	}

	private void progressBarCheck(By step) {
		CompareColorsOfWebElement(step, "rgba(253, 126, 69, 1)");
		print("you are on the right step");
	}

	public void clickNextButton() {
		progressBarCheck(step5);
		progressBarCheck(currentProgressBrick);
		clickOn(nextButton);
		waitForLoaderSpinner();
		progressBarCheck(step6);
	}

	public void clickBackButton() {
		progressBarCheck(step5);
		hoverMouseAndClick(backButton);
		progressBarCheck(step4);
	}

}
