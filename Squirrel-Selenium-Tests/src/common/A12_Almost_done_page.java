package common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class A12_Almost_done_page extends Base {
	// client/onboarding/almost_done
	protected WebDriver driver;

	public A12_Almost_done_page(WebDriver driver) {
		this.driver = driver;
	}

	public void verifyTextOnPage() {
		if (userName.startsWith("invited_from_")) {
			verifyText(container, "Thank you!\n" + "You have completed the initial setup process.\n"
					+ "Your request has now been submitted to one of our Squirrel team. Please allow 1-2 business days and one of our staff will contact you to start the set up of your SMSF.");

		} else {
			verifyText(container,
					"Almost Done!\n" + "You have completed the initial setup process.\n"
							+ "We need your invitees to complete the onboarding process as well.\n"
							+ "Review members onboarding");
		}

		verifyText(toolbar, "Self-Managed Super Fund");
		// verifyText(backButtonText, "Back");
	}

	private void progressBarCheck(By step) {
		CompareColorsOfWebElement(step, "rgba(253, 126, 69, 1)");
		print("you are on the right step");
	}

	public void clickNextButton() {
		// progressBarCheck(step12);
		// progressBarCheck(currentProgressBrick);
		clickOn(nextButton);
		// progressBarCheck(step13);
	}

	public void clickBackButton() {
		progressBarCheck(step12);
		hoverMouseAndClick(backButton);
		progressBarCheck(currentProgressBrick);
		progressBarCheck(step11);
	}

}
