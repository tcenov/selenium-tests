package common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class A10_Payment_info_page extends Base {
	// client/onboarding/payment_info
	protected WebDriver driver;
	private By checkBoxIAgree = By
			.xpath("//div[@class='form-group special-input-container terms-conditions-container']//span");

	public A10_Payment_info_page(WebDriver driver) {
		this.driver = driver;
	}

	public void verifyTextOnPage() {
		verifyText(container, "Your SMSF setup fee\nEstablishment of Self Managed Super Fund:\n"
				+ "$3171.05 (incl. GST)\n" + "Monthly administration fee: $121 (incl. GST)\n"
				+ "This will be billed monthly from your SMSF account.\n" + "Total setup cost:\n"
				+ "$3171.05 (incl. GST)\n" + "I would like to receive my rollover documents by:\n" + "Email\n"
				+ "Post\n" + "You will need to return your rollover forms to us via post at GPO BOX 2725 SYDNEY NSW.\n"
				+ "I agree to Terms and Conditions and Privacy Policy\n" + "Please select your payment method:\n"
				+ "Use my super\n" + "Pay with credit card\n"
				+ "You will not be charged until after we have fully set up your fund.");

		verifyText(toolbar, "Self-Managed Super Fund");
		verifyText(backButtonText, "Back");
	}

	public void selectCheckboxes() {
		clickOn(checkBoxAgree);
		clickOn(checkBoxIAgree);
	}

	private void progressBarCheck(By step) {
		CompareColorsOfWebElement(step, "rgba(253, 126, 69, 1)");
		print("you are on the right step");
	}

	public void clickNextButton() {
		progressBarCheck(step10);
		progressBarCheck(currentProgressBrick);
		clickOn(nextButton);
		print("clicked Next Button");
		waitForLoaderSpinner();
		progressBarCheck(step11);
	}

	public void clickBackButton() {
		progressBarCheck(currentProgressBrick);
		if (userName.startsWith("invited_from_")) {
			progressBarCheck(step8);
			hoverMouseAndClick(backButton);
			print("clicked Back Button");
//			waitForLoaderSpinner();
			progressBarCheck(step7);
		} else {
			progressBarCheck(step10);
			hoverMouseAndClick(backButton);
			print("clicked Back Button");
//			waitForLoaderSpinner();
			progressBarCheck(step9);
		}
		progressBarCheck(currentProgressBrick);
	}

}
