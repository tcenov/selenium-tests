package common;

import org.openqa.selenium.By;

public class Maps {

	By toolbar = By.cssSelector(".SMSF");
	By loginButton = By.cssSelector(".btn.btn-success.btn-block.btn-next.no-margin-top");
	By checkBoxAgree = By.cssSelector(".mr-5");
	By registerButton = By.xpath("//*[contains(text(), 'Register')]");
	By currentProgressBrick = By.cssSelector(".brick.current");
	By loaderSpinner = By.cssSelector(".mat-progress-spinner-indeterminate-animation"); 
	
	
	By button = By.cssSelector(".btn");
	By nextButton = By.cssSelector(".btn");
//	By nextButton = By.xpath("//button[@type='submit']");
	By additionalInfoInputFields = By.cssSelector(".input-container");
	By backButton = By.cssSelector(".mr-2.ng-fa-icon");
	By backButtonText = By.cssSelector(".btn-back");
	// By container = By.xpath("//div[@class='contain-container']");
	By container = By.cssSelector(".contain-container");
	By squirrelContainer = By.cssSelector(".standard-text-block__first");
	By inputs = By.cssSelector(".input-container");
	By inputFields = By.xpath("//input[1]");	
	
	protected By step1 = By.xpath("//div[@class='brick-container d-flex']//div[1]");
	protected By step2 = By.xpath("//div[@class='brick-container d-flex']//div[2]");
	protected By step3 = By.xpath("//div[@class='brick-container d-flex']//div[3]");
	protected By step4 = By.xpath("//div[@class='brick-container d-flex']//div[4]");
	protected By step5 = By.xpath("//div[@class='brick-container d-flex']//div[5]");
	protected By step6 = By.xpath("//div[@class='brick-container d-flex']//div[6]");
	protected By step7 = By.xpath("//div[@class='brick-container d-flex']//div[7]");
	protected By step8 = By.xpath("//div[@class='brick-container d-flex']//div[8]");
	protected By step9 = By.xpath("//div[@class='brick-container d-flex']//div[9]");
	protected By step10 = By.xpath("//div[@class='brick-container d-flex']//div[10]");
	protected By step11 = By.xpath("//div[@class='brick-container d-flex']//div[11]");
	protected By step12 = By.xpath("//div[@class='brick-container d-flex']//div[12]");
	protected By step13 = By.xpath("//div[@class='brick-container d-flex']//div[13]");
	
	
	
	// By login = By.xpath("//button[@class='wrap f15 ng-binding']");
	// By register = By.xpath("//*[contains(text(), 'alabala')]");

	// app-login

	// By.cssSelector(".manual_input_item:nth-of-type(2)"));
	// By test = By.cssSelector(".button.round.orange");
	// By test1 = By.id("idTo");
	// By dame = By.name("");
}
