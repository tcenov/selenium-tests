package common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class A13_Complete_page extends Base {
	// client/onboarding/complete
	protected WebDriver driver;

	public A13_Complete_page(WebDriver driver) {
		this.driver = driver;
	}

	public void verifyTextOnPage() {

		verifyText(container,
				"Thank you!\n" + "You have completed the initial setup process.\n"
						+ "Your request has now been submitted to one of our Squirrel team. Please allow 1-2 business days and one of our staff will contact you to start the set up of your SMSF.\n"
						+ "Go to your progress board");
		verifyText(toolbar, "Self-Managed Super Fund");
		verifyText(backButtonText, "Back");
	}

	private void progressBarCheck(By step) {
		CompareColorsOfWebElement(step, "rgba(253, 126, 69, 1)");
		print("you are on the right step");
	}

	public void clickNextButton() {
		clickOn(nextButton);
		progressBarCheck(step13);
		progressBarCheck(currentProgressBrick);
	}
}
